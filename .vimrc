" coloration syntaxique des th�or�mes BOOST (*.prop) 
au BufRead,BufNewFile *.prop so ~/.vim/syntax/theorem.vim
au BufRead,BufNewFile *.luc so ~/.vim/syntax/lucky.vim

" Pour vim >= 6: les fichier de coloration sont recherch�s automatiquement
" dans ~/.vim/syntax
"au BufRead,BufNewFile *.prop  set ft=theorem
"au BufRead,BufNewFile *.luc   set ft=lucky
"au BufRead,BufNewFile *.saofd set ft=lustre
syntax on         " activation de la coloration syntaxique
set ruler         " affichage de la r�gle en bas
set visualbell    " bip visuel
