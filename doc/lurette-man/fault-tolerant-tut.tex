
% \mynewpage

\section{A small fault-tolerant tutorial}
%\section{A small tutorial illustrating Lurette in action}
\label{running-example}


In this Section, we assume that you have read the Lucky Reference
Manual~\cite{lucky-man} or done the Lucky tutorial, since examples
are given in Lucky. All the files necessary to perform this tutorial
are in the directory \\ {\tt
lurette-V2-xxx/demo-xlurette/fault-tolerant-heater/} of the Lurette
distribution.


\subsection{A fault-tolerant heater controller in Lustre}


We want to test a fault-tolerant heater controller which has three
sensors (namely, three reals inputs) measuring the temperature in a
room, and which returns a Boolean value saying to the heater whether
it should heat or not.

A Lustre implementation of such an heater is provided in the
directory {\tt <arch>/demo-xlurette/fault-tolerant/}.  We only
provide here its (informal) specification, which is enough from the
Lurette black-box testing point of view.

%
% It is based on the following ideas:
%
The main task of that controller is to perform a vote to guess what
the temperature is ({\tt Tguess}). Then, if that guessed temperature
is smaller than a minimum value ({\tt TMIN}), it heats; if it is
bigger than a maximum value ({\tt TMAX}), it does not heat;
otherwise, it keeps its previous state.
%
The voter works as follows: it compares the values of each sensors two
by two, and consider sensors broken as soon as they differ too much.

\begin{example}
  V12 = abs(T1-T2) < DELTA; \commentaire{-- true iff T1 and T2 are valid}
  V13 = abs(T1-T3) < DELTA; \commentaire{-- true iff T1 and T3 are valid}
  V23 = abs(T2-T3) < DELTA; \commentaire{-- true iff T2 and T3 are valid}
\end{example}
%
Hence, there are four cases, depending on the values of  
{\tt V12}, {\tt V13},  and {\tt V23}:
\begin{enumerate}
\item If the three comparisons are valid, it returns the median value
of the three sensors;
\item If only one comparison is false, it considers it as a false
  alarm (e.g., because {\tt DELTA} was too small) and still returns
  the median value.
\item If two comparisons are false (say {\tt V12} and {\tt V13}), it
  deduces the broken sensor ({\tt T1}) and returns the average of the
  other two ({\tt T2+T3/2.0});
\item If the three comparisons are false, it is difficult to know
  whether two or three sensors are broken, and it safely decides not to
  heat in that case.
\end{enumerate}

\paragraph{Technical remark:} 
In order to test that program, there are two things we need to
simulate: the real temperature in the room, and the sensors that
measure that temperature. A priori, the real temperature could be a
variable local to the SUT environment. But, in order to write oracles
that have access to that temperature, we need to add it in the SUT
interface.  That is the (technical) reason why the Lustre program  has an additional input {\tt T}
which it does not use.




\subsection{A first test session using a fake environment}

\begin{figure}[htb]
  \begin{center}
    \texorhtml{\begin{minipage}[c]{0.6\textwidth}\includegraphics[width=\textwidth]{lurette.jpg}\end{minipage}}{\htmlimage{lurette.jpg}}
  \end{center}
  \caption{Lurette snapshot: selecting a SUT and a node with combo-boxes.}
  \label{lurette1}
\end{figure}




We launch the Xlurette tool in the directory containing the SUT: a
snapshot of Xlurette is given in Figure~\ref{lurette1}.  We first
need to fill in the System Under Test fields -- the file name and the
node name -- either manually or via so-called combo boxes
\texorhtml{\includegraphics[height=0.3cm]{combo.jpg}}{\htmlimage{combo.jpg}}. In Figure~\ref{lurette1}, the
SUT is a file named {\tt heater\_control.lus}, with the node {\tt
heater\_control}.

\begin{figure}[htb]
  \begin{center}
    \texorhtml{\begin{minipage}[c]{0.6\textwidth}\includegraphics[width=\textwidth]{lurette1.jpg}\end{minipage}}{\htmlimage{lurette.jpg}}
%     \begin{minipage}[c]{0.6\textwidth}
%       \includegraphics[width=\textwidth]{lurette1.jpg}
%     \end{minipage}
  \end{center}
  \caption{Lurette snapshot: a fake environment has been generated.}
  \label{lurette2}
\end{figure}


Then, we click on the run button
\texorhtml{\includegraphics[height=0.3cm]{run-button.jpg}}{\htmlimage{run-button.jpg}}.
We can observe on the Figure~\ref{lurette2} that the following things
(ought to) happen:

\begin{itemize}
\item The SUT environment field has been filled in with a file named \\
  {\tt heater\_control\_env.luc}
  
\item The test completed, and no property has been violated, which is not too
  surprising since we did not provide any oracle yet.
\end{itemize}




% \mynewpage

\texorhtml{
  \begin{wrapfigure}[13]{r}{7cm}
    \begin{minipage}[c]{8cm}
      \begin{center}
	\begin{small}
	  \begin{example}
	    \input{heater_control_env}
	  \end{example}
	\end{small}
      \end{center}
    \end{minipage}
      \caption{The generated Lucky file.}
      \label{fake-luc}
  \end{wrapfigure}
}{
  \begin{figure}
    \begin{example}
      \input{heater_control_env}
    \end{example}
    \caption{The generated Lucky file.}
    \label{fake-luc}
  \end{figure}
}


The content of that automatically generated environment file is given
in Figure~\ref{fake-luc}.  It is a Lucky program that has:
\begin{itemize}
\item one input variable (the output of the SUT): the Boolean {\tt Heat\_on},
  which is true iff the heater heats;
\item and four output variables (the inputs of the SUT): the true
temperature in the room {\tt T}, as well as the temperature as it is
measured by the 3 sensors: {\tt T1}, {\tt T2}, and {\tt T3}.
\end{itemize}

This Lucky program is rather stupid; at each step, it draws a real
value between 0 and 1 for each of the four outputs. The main
advantage of this generated program is that it provides a good start
for writing a (more) sensible environments for the SUT, as the right
inputs and outputs have been declared. This can be convenient for
programs that have a lot of inputs and outputs.


\begin{figure}[h]
  \texorhtml{
    \begin{center}
      \begin{minipage}[c]{0.6\textwidth}
	\includegraphics[width=\textwidth]{fake.pdf}
      \end{minipage}
    \end{center}
    \vspace{-1cm}
  }{\htmlimage{fake.jpg}
  }
%    \begin{minipage}[c]{0.6\textwidth}
%       \includegraphics[width=\textwidth]{fake.pdf}
%     \end{minipage}
  \caption{The timing diagram of an execution generated with the
    automatically generated environment.}
  \label{fake}
\end{figure}


Now, if we click on the data visualisation button
\texorhtml{\includegraphics[height=0.3cm]{gnuplot-button.jpg}}{\htmlimage{gnuplot-button.jpg}},
we obtain a window similar to the content of Figure~\ref{fake}.



\subsection{A test session using undegradable sensors}


In this Section, we enhance the generated environment and try to
write a Lucky program that generates more realistic input vectors for
the System Under Test.  

%\mynewpage

\texorhtml{
  \begin{wrapfigure}[25]{r}{6cm}
    \begin{minipage}[c]{8cm}
      \begin{small}
	%      \vspace{-1cm}
	\begin{example}
	  \input{sensors}
	\end{example}
      \end{small}
    \end{minipage}
    \vspace{-0.5cm}
    \caption{{\tt sensors.luc}: a Lucky program simulating undegradable sensors.}
    \label{sensors-luc}
  \end{wrapfigure}
}{
  \begin{figure}
    \begin{example}
      \input{sensors}
    \end{example}
    \caption{{\tt sensors.luc}: a Lucky program simulating undegradable sensors.}
    \label{sensors-luc}
  \end{figure}

}

% \begin{figure}[h]
%     \begin{small}
%       \begin{example}
% 	\input{sensors}
%       \end{example}
%     \end{small}
%   \caption{{\tt sensors.luc}: a Lucky program simulating undegradable sensors.}
%   \label{sensors-luc}
% \end{figure}



The Lucky program {\tt sensors.luc} provided in
Figure~\ref{sensors-luc} has the same interface as {\tt
heater\_control\_env.luc}, but in addition, it also have three local
variables ({\tt eps1}, {\tt eps2}, and {\tt eps3}) that are uniformly
drawn between $-0.1$ and $0.1$. Those local variables are used to
disturb the value of the temperature {\tt T} and simulate the noise a
sensor may have ({\tt T1~=~T~+~eps1}).


Then, we need to simulate {\tt T}. To do that, we use two
transitions: the first transition ({\tt 0 -> 1}) initialises the
temperature to $7$. The second transition {\tt 1 -> 1}, from step 2
until the end, updates {\tt T} as follows: if {\tt Heat\_on} is true,
then {\tt T} is incremented of $0.2$; otherwise, it is decremented of
$0.2$.

This model is quite simple, but it will be refined further latter. \\ \\



\begin{figure}[htb]
  \texorhtml{
    \begin{center}
      \begin{minipage}[c]{0.6\textwidth}
	\includegraphics[width=\textwidth]{sensors.pdf}
      \end{minipage}
    \end{center}
  }{
    \htmlimage{sensors.pdf}
  }
  \caption{The timing diagram of an execution generated with the
    undegradable sensors.}
  \label{sensors-pdf}
\end{figure}

An Xlurette run using the Lucky program of Figure~\ref{sensors-luc}
produced the timing diagram of Figure~\ref{sensors-pdf}. There, we
can convince ourselves that everything seems to work correctly; the
temperature increases and {\tt Heat\_on} is true until {\tt TMAX} is
reached. Then, at step 11, {\tt Heat\_on} becomes false and the
temperature decreases until {\tt TMIN} is reached, and so on.


\subsection{Specifying an oracle}

%\begin{center}
  \begin{figure}[htb]
    \begin{small}
      \begin{example}
	\kwd{node} not_a_sauna(T, T1, T2, T3 : \kwdd{real}; Heat_on: \kwdd{bool}) \kwd{returns} (ok:\kwdd{bool});
	\kwd{let} 
	ok = \kwdd{true} -> \kwdd{pre} T < TMAX + 1.0; 
	\kwd{tel}
      \end{example}
    \end{small}
    \caption{The oracle of the test session: make sure that temperature never becomes infernal.}
    \label{sauna}
  \end{figure}
%\end{center}
  

Now that sensible values have been generated, it is time to think
about how to decide automatically if the test succeeded.  The
property that we propose to check is described by the 
Lustre observer of Figure~\ref{sauna} which states that the
temperature should never be bigger than {\tt TMAX+1} even if all
sensors are broken. To take that oracle into account in Xlurette, we
fill the oracle fields in the same manner as for the SUT, using
combo-boxes, as Figure~\ref{lurette3} illustrates.



If we run again our program, we observe that indeed this oracle is
never violated.


\begin{figure}[htb]
  \begin{center}
    \texorhtml{
      \begin{minipage}[c]{0.6\textwidth}
	\includegraphics[width=\textwidth]{lurette2.jpg}
      \end{minipage}
    }{
      \htmlimage{lurette2.jpg}
    }
  \end{center}
  \caption{Lurette snapshot: selecting an oracle.}
  \label{lurette3}
\end{figure}

Note that if you do not specify any oracle, a fake one, named {\tt
always\_true.lus} that always returns {\tt true} is generated. In the
same manner as for the generated environment, this oracle can be used
as a template to write less trivial properties.



\subsection{A test session using degradable sensors}






 \begin{figure}[tbh]
   \texorhtml{
     \begin{center}
       \begin{minipage}[c]{\textwidth}
	 \begin{small}
	   \begin{example}
	     \input{wearing_sensors}
	   \end{example}
	 \end{small}
       \end{minipage}
     \end{center}
     \vspace{-1cm}
   } {
     \begin{example}
       \input{wearing_sensors}
     \end{example}
   } 
   \caption{A Lucky program simulating degradable sensors.}
   \label{wearing-sensors-luc}
 \end{figure}


The Lucky program of Figure~\ref{wearing-sensors-luc} models more
realistic sensors that wears out.
%
The input, output, as well as the {\tt epsi} local variables are the
same ones as in the {\tt sensors.luc} Lucky program of
Figure~\ref{sensors-luc} -- we have omitted them in the Figure for the
sake of conciseness of the code.

There are two additional local variables: {\tt cpt}, that is
incremented at each cycle, and {\tt inv}, an invariant that states
how the temperature {\tt T} is simulated (basically as before) and
how to  update {\tt cpt} at each cycle.

The two transitions {\tt s1 -> t1, t1 -> s1} describes exactly the
same kind of behaviour as the transition {\tt 1 -> 1} in
Figure~\ref{sensors-luc}: {\tt T1}, {\tt T2}, and {\tt T3} are
computed as disturbed version of {\tt T}.
%
Transitions {\tt t2 -> s2, s2 -> t2} simulates the case where one
sensor is broken: {\tt T1} keeps its previous value ({\tt pre T1})
whatever the temperature is.  Transitions {\tt t3 -> s3, s3 -> t3}
and transitions {\tt t4 -> s4, s4 -> t4} respectively simulate cases
where respectively two and three sensors are broken.






Let us run through the execution of that automaton into more detail.
%
The initial node is the one labelled by {\tt t0}.  The output values
for the first cycle are given by the equation that labels the
transition {\tt t1 -> s1}, which states that outputs {\tt T}, {\tt
T1}, {\tt T2}, and {\tt T3}, are set to {\tt 7.0}, and the local
counter {\tt cpt} is set to {\tt 0}.

The values for the second cycle are computed via one of the two
transitions outgoing from node {\tt s1}: {\tt s1 -> t1}, which is
labelled by {\tt 10000}, and {\tt s1 -> t2} which is labelled by {\tt
pre cpt}.
%
The meaning of those weights is the following: use the first
transition with a probability of $\frac{10000}{10000+pre \ cpt}$ and
the second one with a probability of 
$\frac{pre \ cpt}{10000+pre \ cpt}$.  
At second cycle, since {\tt pre cpt} is bound to {\tt 0},
the only possible transition is the second one, which leads to a
correct behaviour of all sensors.


At the third cycle, the situation is roughly the same, except that
the transition {\tt s1 -> t2} is now possible, with a probability of
$\frac{1}{10001}$. If this transition is taken, we enter in a mode
where one sensor is broken.
%
Note that as time flies, the probability to go to the node $t2$
increases; this somehow models that the probability of failure
increases with time.

The behaviour is similar at nodes {\tt s2} and {\tt s3}.  When all
sensors are broken, we go back to the initial state and continue the
test.\\


\begin{figure}[htb]
  \begin{center}
    \texorhtml{
      \begin{minipage}[c]{0.6\textwidth}
	\includegraphics[width=\textwidth]{sensors2_worning.pdf}
      \end{minipage}
    }{
      \htmlimage{sensors2_worning.pdf} 
    }
  \end{center}
  \caption{The timing diagram of an execution generated with the degradable sensors exhibiting the test failure.}
  \label{sensors-worning-pdf}
\end{figure}

\begin{figure}[htb]
  \begin{center}
    \texorhtml{
      \begin{minipage}[c]{0.6\textwidth}
	\includegraphics[width=\textwidth]{lurette3.jpg}
      \end{minipage}
    }{
      \htmlimage{lurette3.jpg}
    }
  \end{center}
  \caption{Lurette snapshot: the oracle has been violated.}
  \label{lurette4}
\end{figure}

If we run the test with {\tt wearing\_sensors.luc} often enough -- or
with a test length that is long enough --, we can exhibit sequences
that violate the oracle. An example of such a sequence is shown in
the timing diagram of Figure~\ref{sensors-worning-pdf}.
 


Indeed, since the way we modelled sensor breakdowns was by making
them keep their previous value, this means that if ever two sensors
broke down with similar values, the voter will not be able to realise
that they are broken, and hence the controller keeps on heating
forever.

One possibility to correct that bug would be to check that sensor
values do change during a given number of cycles, and to consider
them -- at least temporarily -- invalid otherwise.


