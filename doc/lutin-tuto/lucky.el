
;;; lucky.el  -  Major mode for lucky files in Emacs
;;
;; Very very basic: provides only colors for keywords

(require 'font-lock)


; version of lucky-mode
(defconst lucky-mode-version "0.4")


(defvar lucky-mode-map nil
  "Keymap for lucky major mode.")  
 

;;; Font-lock -----------------------------------------------------

(setq comment-start "(*")
(setq comment-end "*)")

(defvar lucky-font-lock-keywords nil
  "Regular expression used by Font-lock mode.")

(defvar lucky-comment-ind-level nil 3)
(setq lucky-font-lock-keywords
      '(("--.*$\\|//.*$" . font-lock-comment-face)
	
	("(\\*\\(.\\|\n\\)*?\\*)" . font-lock-comment-face)
	("\\<\\(weight\\|max\\|min\\|default\\|alias\\|init\\|cond\\|max\\)\\>" . font-lock-type-face)
	("\"\\(.\\|\n\\)*?\"" . font-lock-string-face)


("\\<\\(+\\|*\\|-\\|/\\|mod\\)\\>"
. font-lock-keyword-face)
        ("[][;,()|{}@^!:#*=<>&/%+~?---]\\.?\\|\\.\\."  . font-lock-function-name-face)
        ("\\<\\(true\\|abs\\|and\\|or\\|if\\|then\\|else\\|pre\\|||\\|not\\|false\\|!\\)\\>" . font-lock-reference-face)
	("\\<\\(bool\\|int\\|real\\|float\\)\\(\\^.+\\)?\\>" .  font-lock-variable-name-face)
	("\\<\\(define\\|include\\)\\(\\^.+\\)?\\>" 0 'font-lock-preprocessor-face nil)

	("\\(\\<\\(inputs\\|outputs\\|libraries\\|functions\\|typedef\\|start_state\\|nodes\\|start_node\\|states\\|locals\\|transitions\\|transient\\|final\\|stable\\|arcs_nb\\|nodes_nb\\|=\\)\\>\\|->\\)" . 
font-lock-function-name-face)))



(defun lucky-font-mode ()
  "Initialisation of font-lock for Lucky mode."
  (make-local-variable 'font-lock-defaults)
  (setq font-lock-defaults
        '(lucky-font-lock-keywords t)))

; font-lock isn't used if in a  console
(if window-system
    (prog2
	(add-hook 'lucky-mode-hook
		  'turn-on-font-lock)
	(add-hook 'lucky-mode-hook
		  'lucky-font-mode)))




;;; indentation code ----------------------------------------------
;; copy-pasted from lustre.el; 
;; It does not work as i would like, but better than nothing...

(defun lucky-indent-decl ()
  "Returns the indentation of a declaration line. "
  (interactive)
  (let ((result 2))
    (save-excursion
      (if (re-search-backward "^\\<\\(cond\\|outputs\\|locals\\|nodes\\|start_node\\|transitions\\)\\>" 0 t)
	  (cond
	   ((looking-at "^\\<cond\\>") (setq result 4))
	   ((looking-at "^\\<~\\>") (setq result 6))))
      )
    result))
  


(defun lucky-get-beginning-of-line (&optional arg)
  "Returns position of the first non-space char of the current line,
   or line (arg - 1) if optional argument is given."
  (interactive)
  (save-excursion
    (beginning-of-line arg)
    (let ((deb (point)))
      (skip-chars-forward " \t")
      (let ((fin (point)))
        (- fin deb)))))   

(defun lucky-get-point-of-line ()
  "Returns position of the first char of the current line."
  (interactive)
  (save-excursion
    (beginning-of-line)
    (point)))

(defun lucky-skip-comments ()
  "set point before the commentary of the current line (if any)."
  (interactive)
  (beginning-of-line)
  (while (not (or (looking-at "$")
		  (looking-at "--")))
    (forward-char 1)))
   
(defun lucky-line-is-comment (&optional arg)
  "non-nil means line is only a commentary."
  (interactive)
  (save-excursion
    (beginning-of-line arg)
    (skip-chars-forward " \t")
    (looking-at "--")))

(defun lucky-line-is-decl ()
  "non-nil means current line is a declaration. "
  (interactive)
  (save-excursion
    (let ((res nil)
	  (continue t))
      (while continue
	(if (= (point) 1)
	    (setq continue nil))
	(re-search-backward
	 "\\<\\(inputs\\|outputs\\|locals\\|nodes\\|start_node\\|transitions\\)\\>" 1 t)
	(if (not (lucky-line-is-comment))
	    (setq continue nil)))
      (if (looking-at "\\<\\(~init\\).+\\>")
	  (setq res t))
      res)))


(defun lucky-in-comment ()
  "non-nil means point is inside a comment."
  (interactive)
  (save-excursion
    (re-search-backward "--" (lucky-get-point-of-line) t)))

(defun lucky-skip-commentary-lines ()
  "set point to the beginnig of the first non-commemtary line before
   the current line."
  (interactive)
  (forward-line -1)
  (while (and (lucky-line-is-comment) (> (point) 1))
    (forward-line -1)))

(defun lucky-indent (niveau)
  "Indents current line ."
  (interactive "p")
  (beginning-of-line)
  (delete-char (lucky-get-beginning-of-line))
  (let ((ind niveau))
    (while (> ind 0)
      (insert " ")
      (setq ind (- ind 1)))))


(defun lucky-find-noindent-reg ()
  "non-nil means current line begins with:
  inputs, outputs, locals, nodes, start_node, transitions."
  (interactive)
  (save-excursion
    (beginning-of-line)
    (skip-chars-forward " \t")
    (and
     (looking-at "\\<\\(inputs\\|outputs\\|locals\\|nodes\\|transitions\\)\\>")
     (not (lucky-in-comment)))))


(defun lucky-find-unmatching-parent ()
  "Looks for an unmatched parenthese, and returns its position.
   (or nil if there isn't any). "
  (interactive)
  (let ((continue t)
        (result nil)
        (beg nil)
        (count-parent 0))
    (save-excursion
      (beginning-of-line)
      (if (= (point) 1)
	  (setq continue nil))
      (while continue
        (forward-line -1)
        (setq beg (point))
	(end-of-line)
        (while (and (not (looking-at "^")) continue)
          (if (and (looking-at "\\*)") (not (lucky-in-comment)))
              (setq count-parent (- count-parent 1))
            (if (and (looking-at "(\\*") (not (lucky-in-comment)))
                (progn
                  (setq count-parent (+ count-parent 1))
                  (if (= count-parent 1)
		      (progn
			(setq result (- (point) beg))
			(setq continue nil))))))
	  (forward-char -1))
	(skip-chars-forward " \t")
	(if (and (looking-at "\\<const\\|var\\|type\\|node\\|function\\>")
		 (not (lucky-in-comment)))
	    (setq continue nil))
	(beginning-of-line)
	(if (= (point) 1)
	    (setq continue nil))))
    result))
                    

(defun lucky-indent-normal ()
  "non-nil means indent normally."
  (interactive)
  (save-excursion
    (beginning-of-line)
    (skip-chars-forward "   ")
    (looking-at "[]a-zA-Z0-9~^[()]+")
    )
  )

(defun lucky-empty-line ()
  "non-nil means line is empty."
  (interactive)
  (save-excursion
    (skip-chars-forward " \t")
    (looking-at "$")))


;;; Major-mode

(add-to-list 'auto-mode-alist '("\\.luc$" . lucky-mode))


(defun lucky-mode ()
  " emacs mode for lucky programs "

  (interactive)
  (kill-all-local-variables)
  (setq major-mode 'lucky-mode)
  (setq mode-name "Lucky")
  (use-local-map lucky-mode-map)  
  (make-local-variable 'indent-line-function)
  (run-hooks 'lucky-mode-hook)
)



(provide 'lucky)

;;; lucky.el ends here... 
