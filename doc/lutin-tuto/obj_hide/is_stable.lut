
let min(x, y : real) : real = if x < y then x else y
let max(x, y : real) : real = if x > y then x else y

-- Always loop, even if t deadlocks
let myloop(t:trace) : trace = loop try loop t

-- We flag it as valid after delay consecutive cycles without reset.
-- We reset as soon as diff > epsilon.
node is_d_epsilon_stable(d:int; epsilon, v: real) returns (res:bool) =
  exist vmin, vmax, diff : real in
  let init() = (vmin = v) and (vmax = v) and (diff = 0.0) and not res in
  let step() =  vmin = min(v, pre vmin) and vmax = max(v, pre vmax) and
                diff = ((vmax - vmin) / 2.0)
  in 
   myloop (
     init() fby 
     assert step() and diff <= epsilon in
       loop [d] not res  fby
       loop res
   )

node is_d_m_epsilon_stable(d, m:int ; epsilon, v:real) returns (res:bool) =
  exist stable : bool in
  run stable := is_d_epsilon_stable(d, epsilon, v) in
  myloop (
     loop    { not stable and not res } fby
     loop [m]    { stable and not res } fby  
     strong loop { stable and     res } 
  )

node is_stable(v:real) returns (res:bool) =
  run res := is_d_m_epsilon_stable(3, 5, 3.0, v) 

-- ca sert à rien le is_d_m_epsilon_stable comparé au is_d_epsilon_stable
-- il suffit de prendre d+m et ca fait la meme chose...
--
-- un truc plus interessant, serait de coder un is_3_epsilon_stable exacte,
-- et de l'utiliser dans is_stable


-- exact, and expensive (but for m=3 its ok)
node is_3_stable(epsilon, v:real) returns (vref:real;res:bool) =
 exist v1,v2,v3 : real in

    v1 = v and v2 = v and v3 = v and not res and vref = v
  fby
    assert v1 = v and v2 = pre v1 and v3 = pre v2 and vref = (v1+v2+v3)/3.0 in
    loop [2] not res
    fby
    loop {
      res = abs(v1-v2)<epsilon and
            abs(v1-v3)<epsilon and
            abs(v2-v3)<epsilon 
     }

-- this one is an abstraction
node is_m_stable(d, m:int ; epsilon, v:real) returns (res:bool) =
  exist v_average : real in
  exist vref : real in
  exist is_3_stable : bool in

  run v_average, is_3_stable := is_3_stable(epsilon,v) in 

  myloop (
    loop not is_3_stable and not res and vref = v_average
    fby 
    -- on se souvient de la valeur de ref à l'entree de ce loop pour
    -- détecter une lente dérive de v.
    assert 
      is_3_stable and -- as soon it is false, we start from the beginning
      vref = pre vref and 
      abs(v-vref) < epsilon -- will detect drift
    in
    loop [m] not res fby 
    loop res
  )