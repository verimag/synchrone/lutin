\beamer@endinputifotherversion {3.10pt}
\select@language {french}
\beamer@sectionintoc {1}{Forewords}{3}{0}{1}
\beamer@sectionintoc {2}{Execute Lutin programs}{6}{0}{2}
\beamer@sectionintoc {3}{The Language}{10}{0}{3}
\beamer@sectionintoc {4}{A new operator \texttt {run/in}}{30}{0}{4}
\beamer@sectionintoc {5}{Advanced examples}{33}{0}{5}
