# XXX Edite plutot le main.org ou le (pdf/html-preambule) patate ! 

#+OPTIONS:toc:2
#+TODO: todo  cont  | done 
#+OPTIONS:   \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
 
#+LINK_HOME:http://orgmode.org/worg/org-tutorials/org-beamer/tutorial.html
#+LINK_HOME:http://www-verimag.imag.fr/Lutin.html

#+LINK_UP:http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lurette/doc/lutin-tuto/lutin-tuto-html.html

#+LANGUAGE: en

# INFOJS_OPT: view:nil toc:t ltoc:t mouse:underline buttons:t path:http://orgmode.org/org-info.js
# +INFOJS_OPT: buttons:t view:info toc:  path:http://orgmode.org/org-info.js

#+style: <style type="text/css">pre{background-color: #232323; color: #E6E1DC;}</style>
#+style: <style type="text/css">blockquote{border: 1px solid #CCC; background-color: #E6E1DC; color: #232323;}</style>
#+STYLE: <link rel="stylesheet" type="text/css" href="worg-zenburn.css" />

#+OPTIONS: html-postamble:nil
#+BIND: org-export-html-postamble "<div class='footer'> Built with %c. </div>"

#+SETUPFILE: include/theme-readtheorg.setup

#+MACRO: runpdf  
#+MACRO: run #+begin_src sh \\n<prompt> $2 \n#+end_src
#+MACRO: nprun $2 
#+MACRO: pause 

#+MACRO: begin_html
#+MACRO: end_html 

#+MACRO: onlyhtml $1
#+MACRO: htmlonly $1
#+MACRO: onlypdf 
#+MACRO: pdfonly 
#+MACRO: pdfhtml $2



#+AUTHOR: Erwan Jahier

#+TITLE: A Lutin Tutorial

* Forewords
** Motivations: testing reactive programs

{{{pdfhtml(
#+begin_latex
\input{demarche-it-en}
#+end_latex,
file:./demarche-it-en.png
)}}}


Lutin aims at facilitating the writing of SUT environments, seen as
non-deterministic reactive machines. 


** Lutin in one slide

- Lustre-like: Dataflow, parallelism, modular, logic time, =pre=.
- But not exactly Lustre though
 - Plus
  - *Control structure* operators (regular expressions)
  - *Stochastic* (controlled and pseudo-Aleatory) 

 - Minus
  - No implicit top-level loop 
  - No topological sort of equations

** In order to run this tutorial
 
You first need to install opam. For instance, on debian-like boxes do

#+BEGIN_SRC sh
 sudo apt-get install opam 
 opam init ; opam switch 4.04.0 ; eval `opam config env`
#+END_SRC

 and then do:

#+BEGIN_SRC sh
 sudo apt-get install gnuplot tcl
 opam repo add verimag-sync-repo "http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/opam-repository"
 opam update
 opam install lutin
#+END_SRC

and also the Lustre V4 distribution (for luciole and sim2chro) 
http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lustre-v4/distrib/index.html




Basically, to get all the tools necessary to run this tutorial accessible
from ypour path, you should have something like that in your =.bashrc= :

#+BEGIN_SRC sh
# for lutin, gnuplot-rif, luciole-rif
. ~/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true
# for simec (luciole), sim2chrogtk 
export LUSTRE_INSTALL=~/lustre-v4-xxx
source $LUSTRE_INSTALL/setenv.sh
#+END_SRC


** In order to run the demo from the pdf slides

You can edit (on linux boxes) your =~/.xpdfrc= resource
file modify the =urlCommand= rule:

#+begin_src sh
urlCommand	"browserhook.sh '%s'"
#+end_src

- Download the bash  script  [[http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lurette/sh/browserhook.sh][browserhook.sh  (link  to the  script)]]
- Make it executable and available from your path
- Then the green link should launch the corresponding shell command,
  - A [[http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lurette/doc/lutin-tuto/lutin-tuto-handout.pdf][pdf version of this tutorial]]




{{{runpdf(./sh/echo-go.sh, echo "go!")}}}


* Execute Lutin programs 

# \includegraphics[width=10cm]{file:lutin-luciole.png}

** Stimulate Lutin programs

 

Before  learning the  [[http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lurette/doc/lutin-man.pdf][language]], let's have  a look  at a
couple of tools  that will allow us to play  with our Lutin programs.


 The Lutin  program command-line  interpreter is  a stand-alone
 executable  named ~lutin~.  This program ougth to be 
   accessible from your path if you have followed the instr
 


*** A program that increments its input

Let's consider the following Lutin program named [[./incr.lut][incr.lut]].
#+INCLUDE: "./incr.lut" src lutin

#+begin_src sh :tangle sh/incr-demo.sh :exports none :noweb yes
  xterm -hold -fa "Liberation Mono:size=15:antialias=false" -e "lutin incr.lut"
#+end_src

{{{run(./sh/incr-demo.sh, lutin incr.lut)}}}


#+BEGIN_EXAMPLE
# This is lutin Version 1.54 (9e783b6)
# The random engine was initialized with the seed 817281206
#inputs "x":int 
#outputs "y":int 
#step 1
#+END_EXAMPLE  

This is a [[reactive program]]  that returns its input, incremented by 1,
forever.   We'll explain  why later.   For the  moment,  let's simply
execute it using ~lutin~ from a shell:

At this stage, ~lutin~ waits for an integer input to continue.  If we
feed it with a ~1~ (type ~1<enter>~), it returns 2.  If we enter 41,
it returns 42.  To quit this infinite loop gently, just enter the
character ~q~.

#+BEGIN_SRC sh
<promt> lutin incr.lut
$ 1
1 #outs 2 
#step 2
$ 41
41 #outs 42
#step 3
$ q
q# bye!
#end. 
#+END_SRC



*** A program with no input
 
# Consider the [[./one.lut][one.lut]] program:

#+INCLUDE: "./one.lut" src lutin

#+begin_src sh :tangle sh/one-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 5 one.lut"
#+end_src

{{{run(./sh/one-demo.sh, lutin -l 5 one.lut)}}}


#+BEGIN_SRC sh
# This is lutin Version 1.54 (9e783b6)
# The random engine was initialized with the seed 931220738
#inputs 
#outputs "y":int 
#step 1
#outs 1 
#step 2
#outs 1 
#step 3
#outs 1 
#step 4
#outs 1 
#step 5
#outs 1 
#+END_SRC
A Lutin program might have no input at all. In such a case, it might  
be to helpful  to know that the ~--max-steps~  (~-l~ for short) allows
one  to set a  maximum number  of simulation  steps to  perform.  


#+ATTR_HTML: :border 2 :rules all :frame border :class striped table-striped
#+BEGIN_QUOTE
*Question* : 
 Try to launch ~lutin one.lut -l 5~ on one.lut
#+END_QUOTE




*** Be quiet

#+begin_src sh :tangle sh/one-q-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 5 -quiet one.lut"
#+end_src

{{{run(./sh/one-q-demo.sh, lutin -l 5 -quiet one.lut)}}}


#+BEGIN_SRC sh
1 
1 
1 
1
1 
#+END_SRC
The previous  simulation basically produces a (wordy) sequence of five "1". To
obtain quieter sequence, one can use the ~-quiet~ option (~-q~ for short): 




** Stimulate Lutin programs graphically with ~luciole~

#+INCLUDE: "./incr.lut" src lutin

#+begin_src sh :tangle sh/incr-luciole-demo.sh :exports none :noweb yes
  xterm -e "luciole-rif lutin -rif incr.lut"
#+end_src


# \href{run:./sh/incr-demo.sh}{\texttt{\color{DarkGreen}{ desktop  XXX}}} 


{{{run(./sh/incr-luciole-demo.sh, luciole-rif lutin incr.lut)}}}


It is also  possible to feed Lutin programs  with ~luciole~, a tcl/tk
based GUI that  was originally crafted for Lustre  programs. You will
need to have tcl/tk installed to be able to use it.

[[./jpg/Screenshot-Luciole-incr.jpg]]

The use of Luciole is  straightforward. Some useful features that you
should try to play with are:
- The "Compose" mode (accessible via the "Clocks" menu)
- The "Real Time Clock" mode (accessible via the "Clocks" menu)
- The Sim2chro display (accessible via the "Tools" menu)



** Store and Display the produced data: ~sim2chro~ and ~gnuplot-rif~

*** Generate a RIF file

It is  possible to store the lutin  RIF output into a  file using the
~-o~ option.

#+begin_src sh :tangle sh/rif-demo.sh :exports none :noweb yes
  xterm -e "lutin -l 10 -o ten.rif N.lut"
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "ls -lh ten.rif"
#+end_src

{{{run(./sh/rif-demo.sh, lutin -l 10 -o ten.rif N.lut ; ls -lh ten.rif)}}}

#+INCLUDE: "./N.lut" src lutin






*** Visualize a  RIF file 
#+begin_src sh :tangle sh/cat-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "cat ten.rif"
#+end_src
{{{run(./sh/cat-demo.sh, cat ten.rif)}}}


The ~#outs~, ~#in~, etc., produced by ~lutin~ are RIF directives. RIF
stands for "Reactive Input Format" 




*** Visualize a  RIF file (bis)

#+begin_src sh :tangle sh/sim2chro-demo.sh :exports none :noweb yes
 cat ten.rif | sim2chrogtk -ecran > /dev/null
#+end_src
{{{run(./sh/sim2chro-demo.sh, cat ten.rif | sim2chrogtk -ecran > /dev/null)}}}


In order  to graphically display the  content of this  .rif file, one
can  use  two  tools  that  are part  of  the  ~lutin~  distribution:
~sim2chro~ (or ~sim2chrogtk~), and ~gnuplot-rif~ (requires [[http://www.gnuplot.info/download.html][gnuplot]]).




*** Visualize a  RIF file (ter)

#+begin_src sh :tangle sh/gnuplot-demo.sh :exports none :noweb yes
 gnuplot-rif ten.rif
#+end_src

{{{run(./sh/gnuplot-demo.sh,gnuplot-rif ten.rif)}}}


[[./png/Screenshot-Gnuplot.png]]


** Automatic stimulation of Lutin programs

#+INCLUDE: "./decr.lut" src lutin

#+begin_src sh :tangle sh/lurette-demo.sh :exports none :noweb yes
 lurette -sut "lutin decr.lut -n incr" -env "lutin decr.lut -n decr" -o res.rif &&\ 
 sim2chrogtk -ecran -in res.rif> /dev/null
#+end_src

{{{run(./sh/lurette-demo.sh, 
lurette -sut "lutin decr.lut -n incr" -env "lutin decr.lut -n decr" -o res.rif)}}}

{{{run(./sh/lurette-demo.sh, 
sim2chrogtk -ecran -in res.rif > /dev/null)}}}


{{{htmlonly([[./png/lurette-sim2chro.png]])}}}
 


# \vspace{-4cm}
{{{pdfhtml(
\includegraphics[width=8.5cm]{jpg/geluck-echec.jpg},
file:jpg/geluck-echec.jpg)}}}

- I've bought 2 electronic chess games
- connected one to another
- And now I'm at peace

  
* The Lutin Language


The  aim of this  tutorial is  to be  complementary to  the [[http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lurette/doc/lutin-man.pdf][Reference
Manual]].  The  idea here is to  present the language  via examples. If
you  want  precise definitions  of  the  various language  statements
syntax and semantics, please refer to the [[http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lurette/doc/lutin-man.pdf][Reference Manual]].


** Back to programs of Section 1

- Let's come back to the Lutin  programs mentioned so far. 
#+INCLUDE: "./incr.lut" src lutin


*** We said said that  the  first  one,  [[file:incr.lut][incr.lut]]  increments    its input by one. Let's explain why.


- Those programs illustrate the 2 kinds of expressions we have in Lutin.
  -  *constraint  expressions* (~y = x+1~) that  asserts facts outputs
     variables. 
  - *trace expression* (~loop~ <te>) that allows one to combine 
    constraint expressions.



The behavior  of a /satisfiable/ constraint expression  is to produce
as outputs one  of its possible solutions during  one logical instant
(i.e.,  during one  /step/).  The  behavior of  the  trace expression
=loop te= is to behave as =te= as long as =te= is satisfiable.

Hence, the Lutin program [[file:incr.lut][incr.lut]] could be read like that:

#+begin_quote
I am a reactive program that  takes an integer variable in input, and
that returns  an integer output. As  long as possible, I  will use the
constraint =y = x+1= to produce my output.
#+end_quote

Since this constraint  always has a solution, that  program will loop
forever.   Since  this  constraint  has exactly  one  solution,  this
program is deterministic.


Now let's have a look at the second program we've seen.

#+INCLUDE: "./N.lut" src lutin

The  program [[file:N.lut][=N.lut=]]  illustrates  the use  of  two central  Lutin
keywords: ~fby~ and ~pre~.

~fby~ is the trace sequence  operator.  =b1 fby b2= means: behaves as
~b1~, and  when no more behavior  is possible, behaves  as ~b2~.  The
constraint ~y=0~ is  always satisfiable; its behavior is  to bind the
output  ~y~ to  0 for  one step.   ~y=0 fby  loop y  = pre  y+1~ will
therefore behave  as ~y=0~ for the first  step, and as ~loop  y = pre
y+1~ for the remaining steps.

~pre~ is the  delay operator (as in Lustre). =pre  y= is an expression
that is bound to the value of =y= at the previous instant.   =y =
pre y+1= will  therefore increment the value of  =y= at each instant.
Again this program is deterministic and runs infinitely.

Of  course, this means  that =y=  should be  defined at  the previous
instant. This  is the reason  why we've distinguished the  first step
from the  others. However, it  is possible to set  variables previous
values at  declarations time.  The program  [[file:N2.lut][=N2.lut=]] below behaves
exactly as [[file:N.lut][=N.lut=]]:

#+INCLUDE: "./N2.lut" src lutin  

#+BEGIN_QUOTE
*Question*: 
Write a Lutin program with no input that generates one output =y=
with  the following sequence of values : 1 3 5 7 9 ... 
#+END_QUOTE
#+BEGIN_QUOTE
*Question*: 
Write a Lutin program with no input that generates one output =y=
with  the following sequence of values : 1 2 4 8 16 32 ... 
#+END_QUOTE



** Non deterministic programs

*** A First non-deterministic program


#+INCLUDE: "./trivial.lut" src lutin

#+begin_src sh :tangle sh/trivial-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 10 -q trivial.lut"
#+end_src

{{{run(./sh/trivial-demo.sh, lutin -l 10 -q trivial.lut)}}}


#+begin_example
<prompt>  lutin -l 10 -q trivial.lut
129787787 89770690.47 f 
-202124065 60018934.21 t 
-74063075 -224861409.44 t 
-120351222 37052102.10 t 
-84618074 238667445.54 t 
186689955 -188684349.70 t 
211369478 -253941536.69 t 
-135268511 52434720.53 t 
113139954 241863809.02 t 
-114970522 -70907151.75 t 
#+end_example

The programs  we've seen  so far were  deterministic because  all the
constraint expression were  affectations (i.e., equalities between an
input  and  an  output  variables).   As a  matter  of  fact,  writing
non-deterministic  is  even  simpler:  you  just  need  to  write  no
constraint at all!  Indeed, observe how this [[file:trivial.lut][trivial.lut]] program
behaves:
 



*** It is possible to set the variable range at declaration time,
as done in [[file:trivial2.lut][trivial2.lut]]:

#+INCLUDE: "./trivial2.lut" src lutin


#+begin_src sh :tangle sh/trivial2-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 10 -q trivial2.lut"
#+end_src

{{{run(./sh/trivial2-demo.sh,lutin -l 10 -q trivial2.lut)}}}


#+begin_example
71 42.08 f 
-61 27.28 f 
-49 -64.96 f 
44 63.08 t 
-15 73.06 f 
0 -74.39 f
87 -1.58 f 
-20 89.62 f 
3 -84.20 t 
89 28.64 f 
#+end_example


** Non deterministic programs (cont)
 As you've just seen, a  Lutin program is by default chaotic. To
make it less chaotic, one has to add constraints which narrow the set
of  possible behaviors.   When  only one  behavior  is possible,  the
program is said to be /deterministic/.  When no behavior is possible,
the program stops.


 Now consider the [[file:range.lut][range.lut]] program:

#+INCLUDE: "./range.lut" src lutin

#+begin_src sh :tangle sh/range-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin range.lut -l 10 -q"
#+end_src

{{{run(./sh/range-demo.sh, lutin range.lut -l 10 -q)}}}

 The  constraint expression  ~0 <=  y and y  <= 42~  has several
solutions. One of  those solutions will be drawn  and used to produce
the output of the current step.

#+begin_example
<prompt> lutin range.lut -l 10 -q
24 
38 
12 
30 
5 
5 
40 
8 
26 
39 
#+end_example

Lutin constraints  can only  be linear, which  means that the  set of
 solutions  is a union  of convex  polyhedra. Several  heuristics are
 defined to  perform the  solution draw, that  are controlled  by the
 following options:



{{{onlypdf(
- Linear constraints \to union  of convex polyhedra
- Several heuristics are defined to perform the solution  draw
)}}}

 - =--step-inside= (=-si=): draw inside the polyhedra 
  (the default)
 - =--step-vertices= (=-sv=) draw among the polyhedra vertices 
 - =--step-edges= (=-se=): promote edges 


#+begin_src sh :tangle sh/range-sv-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin range.lut -q -l 10 -sv "
#+end_src

{{{run(./sh/range-sv-demo.sh, lutin range.lut -l 10 -q --step-vertices)}}}


#+begin_example
42 
0 
42 
42 
0 
42 
42 
0 
0 
42 
#+end_example


#+BEGIN_QUOTE
*Question*: write  a simpler  program than [[file:range.lut][range.lut]]  that behaves
the same.
#+END_QUOTE


{{{onlyhtml([[file:range2.lut][Answer]])}}}

** Non deterministic programs (cont)

- A 3D non-deterministic example 

{{{onlyhtml(Of course constraints can be more complex as in [[file:polyhedron.lut][polyhedron.lut]]:)}}}

#+INCLUDE: "./polyhedron.lut" src lutin

#+begin_src sh :tangle sh/poly-demo.sh :exports none :noweb yes
  xterm  -e "lutin polyhedron.lut -l 1000 -quiet > poly.data";
  echo 'set pointsize 0.2 ; splot "poly.data" using 1:2:3;pause mouse close'| gnuplot
#+end_src

{{{run(./sh/poly-demo.sh, lutin polyhedron.lut -l 1000 -q > poly.data;\\ 
echo 'set point 0.2; splot "poly.data" using 1:2:3;pause mouse close'| gnuplot)}}}


However, constraint expressions should  be linear.  For example, =x *
x >0= is rejected by the current  solver.  But of course, =x * y > 0=
is ok if =x= (or =y=) is an input.

=gnuplot-rif= is  not designed to  display 3d data; however,  one can
generate and  visualize data generated  by this program using  the 3D
plotting facilities of gnuplot like that:




#+begin_src sh :tangle sh/poly-se-demo.sh :exports none :noweb yes
  xterm -e "lutin polyhedron.lut -se -l 1000 -quiet > poly-se.data" &&
  echo 'set point 0.2 ; splot "poly-se.data" using 1:2:3; pause mouse close'| gnuplot
#+end_src

#+begin_src sh :tangle sh/poly-sv-demo.sh :exports none :noweb yes
  xterm -e "lutin polyhedron.lut -sv -l 1000 -quiet > poly-sv.data" &&
  echo 'set point 0.8; splot "poly-sv.data" using 1:2:3; pause mouse close'| gnuplot
#+end_src


- One   can   observe   the    effect   of  
    {{{nprun(./sh/poly-se-demo.sh,--step-edges)}}}
and
{{{nprun(sh/poly-sv-demo.sh,--step-vertices)}}})
options on the repartition of generated points



#+begin_example
<prompt> lutin polyhedron.lut -l 1000 -quiet -se > poly-se.data 
<prompt> lutin polyhedron.lut -l 1000 -quiet -sv > poly-sv.data 
<prompt> echo 'set pointsize 0.2; splot "poly-se.data" using 1:2:3;pause mouse close'| gnuplot
<prompt> echo 'set pointsize 0.8; splot "poly-sv.data" using 1:2:3;pause mouse close'| gnuplot
#+end_example

hint: use the mouse inside the gnuplot window to change the perspective.

file:./png/Screenshot-Gnuplot-se.png


** Non deterministic programs (cont)


Constraint   may   also  depend   on   inputs.   


Try   to  play   the [[file:range-bis.lut][range-bis.lut]] program:

#+INCLUDE: "./range-bis.lut" src lutin
#+begin_src sh :tangle sh/range-bis-demo.sh :exports none :noweb yes
  xterm  -e "luciole-rif lutin range-bis.lut"
#+end_src

{{{run(./sh/range-bis-demo.sh, luciole-rif lutin range-bis.lut)}}}



The  major  difference with  [[file:range.lut][range.lut]]  is  that the  constraint
expression ~0  <= y and  y <= i~  is not always satisfiable.   If one
enters a negative value, that program will stop.


#+BEGIN_QUOTE
*Question:*  modify the  [[file:range-bis.lut][range-bis.lut]] program  (with  the concept
introduced  so far) so  that when  a negative  input is  provided, it
returns -1.

#+END_QUOTE

[[file:answer1.lut][Answer]]


** Controlled non-determinism: the choice operator

[[file:choice.lut][choice.lut]]

#+INCLUDE: "./choice.lut" src lutin

#+begin_src sh :tangle sh/choice-demo.sh :exports none :noweb yes
  xterm  -hold  -fa "Liberation Mono:size=15:antialias=false" -e "lutin -l 10 -q choice.lut";
#+end_src

{{{run(./sh/choice-demo.sh, lutin -l 10 -q choice.lut)}}}

#+begin_example
42 
42 
42 
1 
42 
42 
42 
42 
1 
1 
#+end_example
In  the  previous  programs,  it  is  not  possible  to  control  the
non-determinism. It is possible  to change the drawing heuristics,
but that's all.   In order to control the  non-determinism, one as
to use the choice operator (=|=).

When executing the [[file:choice.lut][choice.lut]],  the Lutin interpreter performs a
fair choice  among the 2 satisfiable  constraints (~x = 42~  and ~x =
1~).



It is possible to favor one branch over the other using 
weight directives (=:3=):

#+INCLUDE: "./choice2.lut" src lutin

In [[file:choice2.lut][choice2.lut]], ~x=42~ is chosen with a probability of 3/4.

 
#+begin_src sh :tangle sh/choice2-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 10000 -q choice2.lut | grep 42 | wc -l";
#+end_src

{{{run(./sh/choice2-demo.sh, lutin -l 10000 -q choice2.lut | grep 42 | wc -l)}}}


#+begin_example
7429
#+end_example

nb: "|" is a shortcut for "|1:". 

nb 2: having 3 choices with a weight of 1, that does not necessarily
means that each branch is  chosen with a probability of 1/3.  Indeed,
the  choice in done  among satisfiable  constraints. For  instance in
[[file:choice3.lut][choice3.lut]] below,  not all the branches of  the alternative can
be satisfiable.


** Controlled non-determinism: the choice operator

#+INCLUDE: "./choice3.lut" src lutin
#+begin_src sh :tangle sh/choice3-demo.sh :exports none :noweb yes
  xterm  -e "luciole-rif lutin choice3.lut";
#+end_src

{{{run(./sh/choice3-demo.sh, luciole-rif lutin choice3.lut)}}}



** Combinators

 A /combinator/ is a well-typed  macro that eases code reuse.  
 One can define  a combinator with the =let/in=  statement, or just
 =let= for top-level combinators.


*** A simple combinator

#+INCLUDE: "./letdef.lut" src lutin

#+begin_src sh :tangle sh/letdef-demo.sh :exports none :noweb yes
xterm -hold  -fa "Liberation Mono:size=15:antialias=false" -e "lutin -quiet letdef.lut"
#+end_src

{{{run(./sh/letdef-demo.sh, lutin -quiet letdef.lut)}}}

** A parametric combinator 

The [[file:combinator.lut][combinator.lut]] program illustrates the use of parametric combinators:

#+INCLUDE: "./combinator.lut" src lutin

#+begin_src sh :tangle sh/combinator-demo.sh :exports none :noweb yes
rm -f walk.rif ; lutin  -l  100  combinator.lut  -o walk.rif && \
gnuplot-rif walk.rif
#+end_src

{{{run(./sh/combinator-demo.sh, lutin -l 100 combinator.lut -o walk.rif ; gnuplot-rif walk.rif)}}}


file:png/Screenshot-walk.png

#+BEGIN_QUOTE
*Question:* Write such a randow walk for a real variable
#+END_QUOTE


** Combinators  (cont)

*** A combinator that needs memory   (=ref=)  


If one  wants to access to the  previous value of a  variable inside a
combinator, one  has to declare  in the combinator profile  that this
variable is a /reference/ using  the =ref= keyword, as illustrated in
the [[file:up-and-down.lut][up-and-down.lut]] program:


#+INCLUDE: "./up-and-down.lut" src lutin

#+begin_src sh :tangle sh/up-and-down-demo.sh :exports none :noweb yes
luciole-rif lutin up-and-down.lut
#  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin up-and-down.lut"
#+end_src
#+begin_src sh :tangle sh/gnuplot-rif-luciole.sh :exports none :noweb yes
#  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "
gnuplot-rif luciole.rif
#+end_src

{{{run(./sh/up-and-down-demo.sh, luciole-rif lutin up-and-down.lut)}}}

{{{run(./sh/gnuplot-rif-luciole.sh, gnuplot-rif luciole.rif)}}}

 The combinator =up=  (reps =down=) constraints the variable =x=
 to be between  its previous value and its  previous value plus (resp
 minus)  a  positive  =delta=.   The  node  =up-and-down=,  after  an
 initialization  step where  =x= is  drawn between  =min=  and =max=,
 chooses (fairly) to go  up, or to go down.  It it  chooses to go up,
 it does so as long as =x=  is smaller than =max=; then it goes down,
 until =min=, and so on.




 
#+BEGIN_QUOTE
*Question:* what  happens if you  guard the =up= combinator  by =x<max=
instead of =pre x < max=?
#+END_QUOTE

** Local variables
   
Sometimes,  it is  useful to  use  auxiliary variables  that are  not
output  variables.    Such  variables  can  be   declared  using  the
~exist/in~    construct.    Its   use    is   illustrated    in   the
[[file:true-since-n-instants.lut][true-since-n-instants.lut]] program:

#+INCLUDE: "./true-since-n-instants.lut" src lutin
#+begin_src sh :tangle sh/true-since-demo.sh :exports none :noweb yes
luciole-rif lutin true-since-n-instants.lut
#+end_src

{{{run(./sh/true-since-demo.sh, luciole-rif lutin true-since-n-instants.lut)}}}


It is possible  to set its previous value at  declaration time as for
interface variables. Here, the previous value of local variable ~cpt~
is set to the constant n, which is bound to 3. This local variable is
used to count the number  of consecutive instants where the input ~b~
is  true.   ~cpt~  is reset  to  ~n~  each  time  ~b~ is  false,  and
decremented otherwise. The node returns true when ~cpt~ is smaller or
equal than 0.



** Local variables again

The  previous example  was deterministic  (it was  actually  a Lustre
program with  an explicit  ~loop~), the local  variable was  a simple
(de)counter.  

Local variables  can also  plain random  variables, as
illustrated the [[file:local.lut][local.lut]] program:

#+INCLUDE: "./local.lut" src lutin

 At first  step, the local variable ~target~  is chosen randomly
 between  0.0 and 42.0,  and x  keeps its  previous value  (0); then,
 during 20 steps, the output x  comes closer and closer to target (~x
 = (pre x + target) / 2.0~), while ~target~ keeps its previous value.
 After 20 steps (~loop [20])~), another value for ~target~ is chosen,
 and so on so forth (because of the outer ~loop~).


#+begin_src sh :tangle sh/local-demo.sh :exports none :noweb yes
 rm -f local.rif; lutin local.lut -l 100 -o local.rif && gnuplot-rif local.rif
#+end_src
#+begin_src sh :tangle sh/local-damped.sh :exports none :noweb yes
 rm -f local-bis.rif; lutin local-bis.lut -l 100 -o local-bis.rif && gnuplot-rif local-bis.rif
#+end_src

{{{run(./sh/local-demo.sh,lutin local.lut -l 100 -o local.rif ; gnuplot-rif local.rif)}}}


file:png/Screenshot-Gnuplot-local.png


 *Question:* modify  the previous program  so that x reaches  the target
    after a
  {{{nprun(./sh/local-damped.sh,damped oscillation)}}}




like in the following screen-shot:

file:png/Screenshot-Gnuplot-localbis.png


** Damped oscillation
#+INCLUDE: "./local-bis.lut" src lutin

** Distribute a constraint into a scope: =assert=


 Now  let's consider  a slightly  different version  of  the previous
 program  where ~n~  is  an input  of  the node.   Since the  current
 version of  the Lutin  interpreter is not  able to set  the previous
 value  of a  variable with  an input  value (this  restriction might
 change in  the future),  we need  to write for  the first  instant a
 constraint that does not involve ~pre cpt~.


Consider for instance  the  [[file:true-since-n-instants2.lut][true-since-n-instants2.lut]] program:

#+INCLUDE: "./true-since-n-instants2.lut" src lutin

- One flaw is that ~res = (b and (cpt<=0))~ is duplicated.

 This occurs very often, for example when you want to a variable
 to keep its  previous value during several steps,  and need to write
 boring ~X = pre X~ constraint  all the time. Indeed in Lutin, if one
 says nothing about a variable, it is chosen randomly. 

- The ~assert/in~ construct has been introduced in Lutin to avoid such
    code duplication.  

 
- ~assert <ce> in <te>~ \equiv ~<te'>~, 

     where <te'>= <te>[c/c and ce]$_{\forall c \in \mathcal{C}onstraints(te)}$

 

i.e., where the  trace expression ~<te'>~ is obtained  from the trace
expression  ~<te>~  by substituting  all  the constraint  expressions
~<c>~  appearing in  ~<te>~  by the  constraint  expression ~<c>  and
<ce>~.



#+BEGIN_QUOTE
 *Question:* Rewrite the [[file:true-since-n-instants2.lut][true-since-n-instants2.lut]] using the
   ~assert/in~ construct and avoid code duplication.
#+END_QUOTE
  
 [[file:true_since_n_instants3.lut][Answer]]

** External code

Lutin program can call any function defined in a shared library (=.so=)

#+begin_src lutin :tangle ext-call.lut :exports none :noweb yes
extern sin(x: real) : real
let between(x, min, max : real) : bool = ((min < x) and (x < max))
node bizzare() returns (x,res: real) = 
  exist noise: real in
  assert between(noise,-0.1, 0.1) in
  res = 0.0 and x = 0.0 fby 
  loop     x = pre x + 0.1 + noise
       and res = sin(pre x)
#+end_src

#+INCLUDE "ext-call.lut" src lutin

#+begin_src sh :tangle sh/ext-call-demo.sh :exports none :noweb yes
 rm -f ext-call.rif
 lutin -L libm.so -l 200 -o ext-call.rif ext-call.lut && \
 gnuplot-rif ext-call.rif
#+end_src

{{{run(./sh/ext-call-demo.sh, lutin -L libm.so -l 200 ext-call.lut -o ext-call.rif;gnuplot-rif ext-call.rif)}}}


file:png/Screenshot-Gnuplot-ext.png


** Exceptions
*** Global exceptions can be declared outside the main node:

#+begin_src lutin
  exception ident
#+end_src 

*** or locally within a trace statement:
#+begin_src  lutin
exception ident in st
#+end_src

*** An existing  exception ident can  be raised with the  statement: 
#+begin_src lutin
raise ident
#+end_src

*** An exception can be caught with the statement:
#+begin_src lutin
catch ident in st1 do st2
#+end_src

If the exception is raised  in st1, the control immediatelly passes to
st2. If the "do" part is omitted, the statement terminates normally.

** Exceptions (cont)

- The predefined Deadlock exception can only be catched

#+begin_src lutin 
  catch Deadlock in st1 do st2
#+end_src
\equiv
#+begin_src lutin 
  try st1 do st2
#+end_src


- When  a   trace  expression  deadlocks,  
  the  Deadlock  exception is  raised.  In  fact,  this exception  is
  internal and cannot be redefined  nor raised by the user.  The only
  possible use of the Deadlock in programs is one try to catch it:


- If a deadlock is raised during the execution of st1, the control
  passes immediately to st2.   If st1 terminates normally, the whole
  statement terminates and the control passes to the sequel.  

** Exceptions (cont)


#+INCLUDE "except.lut" src lutin
#+begin_src sh :tangle sh/except-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin except.lut"
#+end_src


{{{run(./sh/except-demo.sh, luciole-rif lutin except.lut)}}}


 [[file:except.lut][except.lut]]


Note that the 43 value is  generated iff i=43.


** Combinators (again)
*** Trace Combinators

#+begin_src lutin
let myloop(t:trace) : trace = loop try loop t
#+end_src

Here we restart the loop from the beginning whenever we are
blocked somewhere inside =t=. ([[file:myloop.lut][myloop.lut]])

#+INCLUDE: "./myloop.lut" src lutin
#+begin_src sh :tangle sh/myloop-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin myloop.lut"
#+end_src

{{{run(./sh/myloop-demo.sh, luciole-rif lutin myloop.lut)}}}


Each step you set reset to =true=, the output equals to =0=.
 



** Parallelism: =&>=

# file:paralel.lut

#+INCLUDE: "./paralel.lut" src lutin 6-

#+begin_src sh :tangle sh/para-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin paralel.lut"
#+end_src

{{{run(./sh/para-demo.sh, luciole-rif lutin paralel.lut)}}}
 

=<te1> &> <te2>= executes the trace expression =<te1>= and =<te2>= in
 parallel.

 
=&>st1  &>...  &>stn=  where  the  first =&>=  can  be omitted.   This
statement  executes  in  parallel   all  the  statements  =st1=,  ...,
=stn=. All along  the parallel execution each branch  produces its own
constraint;  the  conjunction of  these  local  constraints gives  the
global  constraint.   If one  branch  terminates  normally, the  other
branches continue.   The whole statement terminates  when all branches
have terminated.



nota bene: this construct can be expensive because of: 
- *the  control  structure*: such  a  product is  equivalent to  an
  automata product, which, in the worst case, can be quadratic;
- *the  data*:  the  polyhedron resolution  is  exponential in  the
  dimension of the polyhedron.
  
Use the =run/in= construct instead if performance is a problem.



# ** Parallelism *)

# In Lutin,  there are two  operators for computing things  in parallel: *)
# =&>= and =run=. *)

* The =run= operator  
** Cheap parallelism: Calling Lutin nodes =run/in=

 The idea  is the following: when one writes:

#+begin_src lutin
  run (x,y) := foo(a,b) in
#+end_src

in order to be accepted the following rules must hold:
- =a= and =b= be uncontrollable variables (e.g., inputs or memories)
- =x= and =y= should be controllable variables (e.g., outputs or locals)
- in the scope of such a =run/in=, =x= and =y= becomes uncontrollable.
 

nb : it is exactly the parallelism of Lustre, with an heavier syntax.
In Lustre, one would simply write 
#+begin_src lustre
 (x,y)=foo(a,b);
#+end_src

Moreover in Lutin, the order of equations matters.

** Cheap parallelism: Calling Lutin nodes =run/in=

- The =run/in= construct is another (cheaper) way of executing code in parallel
- The only way of calling Lutin nodes.  
- Less powerful: constraints are not merged, but solved in sequence 

#+INCLUDE: "./run.lut" src lutin

#+begin_src sh :tangle sh/run-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 5 -q run.lut -m use_run"
#+end_src

{{{run(./sh/run-demo.sh,lutin -l 5 -q run.lut -m use\_run)}}}


#+begin_example
2 
3 
4 
5 
6 
#+end_example

The program [[file:run.lut][run.lut]] illustrates the use of the =run/in= statements:
This program uses nodes defined in [[file:N.lut][=N.lut=]] and =[[file:incr.lut][incr.lut=]].


Another illustration of the use of  =run= can be found in the [[Wearing
sensors]] exemple.






# ** Expensive versus cheap parallelism

#** Nodes versus combinators 
   
** Why does the =run/in= statement is important?
   
Using combinators and =&>=, it was already possible to reuse code, but
=run/in= is

- Much more efficient: polyhedra dimension is smaller
  
#Consider this example : xxx

- Mode-free (args can be in or out) combinators are error-prone
#Consider this example : xxx

 
* Advanced examples 
    
** Wearing sensors

The [[file:sensors.lut][sensors.lut]]  program that  makes extensive use  fo the
run statements.

#+INCLUDE: "./sensors.lut" src lutin


#+begin_src sh :tangle sh/sensors-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin sensors.lut -m main"
#+end_src

{{{run(./sh/sensors-demo.sh,luciole-rif lutin sensors.lut -m main)}}}


#+begin_example
<prompt> luciole-rif lutin sensors.lut -m main 
#+end_example


** Waiting for the stability of a signal


- One of  the target application of Lutin is  the programming of test
  scenario of  control-command system.  In this context,  it is often
  very useful  to wait  for the stability  of a  variable (typically,
  coming from a sensor) before changing again the value associated to
  a  command.  


- Defining and checking the stability of a variable (in particular in
  presence of noise) is not that easy.

- One  definition could be that  a variable is stable  if it remains
  within  an  interval  during  a  certain  amount  of  time.   More
  precisely:


#+begin_quote
A variable V  is (d,\epsilon)-stable at instant i  if there exists an
interval I of size \epsilon, such that, for all n in [i-d,i], V(n) is
in I.

\exists I, st |I| = \epsilon, \forall n \in [i-d, i] V(n) \in I
#+end_quote



- In  order to  implement such  a definition, we'd  need to  manage n
   variables; and in order to  get parametric code, we'd need arrays.
   In any  case, this is quite  expensive (O(n)).  Here  we propose a
   lighter version of this test, that is based on the following idea:
   we test for the stability of the variable during a small amount of
   instants and  hence obtain  an interval I;  then, we  simply check
   that the variable  remains in I for the  n-m+1 remaining instants.
   This is less expensive (O(m) versus O(n)), but of course it is not
   optimal in the sense that in some cases, we might detect 
   stability a little bit later.

- We  now propose a Lutin  and a Lustre implementation  of this idea.
  The Lutin program is fully deterministic, but interestingly enough,
  one  might find  it easier  (or not...)   to read  than  the Lustre
  equivalent program.   This is because  Lustre is a  purely dataflow
  programming  language, while  Lutin do  have explicit  control flow
  operators (=fby=, =loop=). In  the Lustre version of =compute_ref=,
  we need to encode a 2-states automaton.  Such an encoding makes the
  temporal behavior of the program less explicit.

*** The Lutin version

We  now  paraphrase  the  [[file:is_stable.lut][is_stable.lut]] Lutin  version  of  this
algorithm.

First we define a few constants:
#+INCLUDE: "./is_stable.lut" src lutin :lines "1-4"

and a few macros:

#+INCLUDE: "./is_stable.lut" src lutin :lines "5-10"

 Then we  define the node  =compute_ref= that search  for (=epsilon=,
 =small_delay=)-stability.   This node returns  a value  of reference
 =vref= that  is equal  to the middle  of I  of a suitable.   It also
 returns  a  boolean  =valid= that  becomes  true  as  soon as  v  is
 (=epsilon=, =small_delay=)-stable, and that  remains true as long as
 v remains in I. Note that I is re-initialiazed as soon as it becomes
 bigger that =epsilon=.

#+INCLUDE: "./is_stable.lut" src lutin :lines "11-30"

 The node =is_stable= rests  on the =compute_ref= node.  When =valid=
 becomes  true, it waits  =big_delay= instants,  and returns  true as
 long as =valid= remains true.

#+INCLUDE: "./is_stable.lut" src lutin :lines "31-"



[[file:is_stable.lut][The Lutin version]]

#+begin_src sh :tangle sh/is_stable-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin is_stable.lut -m is_stable"
#+end_src

{{{run(./sh/is_stable-demo.sh,luciole-rif lutin is\_stable.lut -m is\_stable)}}}


*** The Lustre version :noexport:

Now, let's  write the same  thing in Lustre,  just for the fun  of it
(and also because the notion  of stability can be useful when writing
test oracles, that we traditionally write in Lustre).


[[file:is_stable.lus][The Lustre version]]


** The Crazzy rabbit :noexport:
   
XXX pour  l'instant je  ne sais pas  a appeler du  ocaml via  rdbg en
ligne  de commande.  Et ai-je  vraiment  envie de  le faire  ? Ca  va
compliquer rdbg.ml grave. Bref, pour refaire marcher ce truc, il faudra
que j'ecrire une session.ml a la main

- The rabbit serves as an environment for a caml program
  that displays its position in a graphical windows
- The rabbit remains in its field, and avoids a moving obstable
- The rabbit changes it speed an trajectory from times to times

#+begin_src sh :tangle sh/rabbit-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "cd crazy-rabbit; lurettetop -rp "sut:ocaml:rabbit.cmxs:" -rp 'env:lutin:rabbit.lut:-main:rabbit:-L:libm.so:-loc'"
lurette -sut "" -env "lutin crazy-rabbit/rabbit.lut -n rabbit -L libm.so"

#+end_src

{{{run(sh/rabbit-demo.sh,lurettetop -rp "sut:ocaml:rabbit.cmxs:" -rp 'env:lutin:rabbit.lut:-main:rabbit:-L:libm.so:-loc')}}}


[[file:crazy-rabbit/ud.lut][crazy-rabbit/ud.lut]]

#+INCLUDE: "./crazy-rabbit/ud.lut" src lutin

[[file:crazy-rabbit/moving-obstacle.lut][crazy-rabbit/moving-obstacle.lut]]
# #+INCLUDE: "./crazy-rabbit/moving-obstacle.lut" src lutin

[[file:crazy-rabbit/rabbit.lut][crazy-rabbit/rabbit.lut]]
#+INCLUDE: "./crazy-rabbit/rabbit.lut" src lutin
# file:png/Screenshot-crazzy.png


- [[file:crazy-rabbit/ud.lut][crazy-rabbit/ud.lut]]
- [[file:crazy-rabbit/moving-obstacle.lut][crazy-rabbit/moving-obstacle.lut]]
- [[file:crazy-rabbit/rabbit.lut][crazy-rabbit/rabbit.lut]]
- [[file:crazy-rabbit/rabbit.ml][crazy-rabbit/rabbit.ml]]

  
