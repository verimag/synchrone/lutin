# XXX Edite plutot le main.org patate ! 
# Time-stamp: <modified the 20/03/2017 (at 22:16) by jahier>
#+OPTIONS: LaTeX:t  
#+OPTIONS:toc:2
#+TODO: todo  cont  | done 

#+OPTIONS: H:3 num:t toc:t \n:nil  ::t |:t ^:t -:t f:t *:t skip:t tags:not-in-toc

#+LINK_UP: http://orgmode.org/worg/org-tutorials/org-beamer/tutorial.html
#+LINK_HOME:http://www-verimag.imag.fr/Lutin.html
#+STYLE: <link rel="stylesheet" type="text/css" href="slides.css" />

# ## http://orgmode.org/worg/org-tutorials/org-beamer/tutorial.html
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [nowasysym]
#+LaTeX_CLASS_OPTIONS: [nomarvosym]
#+BEAMER_FRAME_LEVEL: 2

#+BEAMER_HEADER_EXTRA: \usefonttheme{structurebold}

#+LANGUAGE: en
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
#+LATEX_HEADER: \usepackage{color}
#+LATEX_HEADER: \usepackage{xcolor}
#+LATEX_HEADER: \usepackage{listings}
#+LATEX_HEADER: \input{mybeamer}

#+AUTHOR: \href{grille.pdf}{Erwan Jahier}
#+TITLE: \href{lutin-tuto-pdf.pdf}{A Lutin Tutorial}



#+MACRO: runpdf   #+begin_latex\n\href{file:$1}{\texttt{\color{DarkGreen}{<prompt> $2}}} \n#+end_latex
#+MACRO: run   #+begin_latex\n\href{file:$1}{\texttt{\color{DarkGreen}{<prompt> $2}}} \n#+end_latex
#+MACRO: nprun #+begin_latex\n\href{file:$1}{\texttt{\color{DarkGreen}{$2}}}          \n#+end_latex
#+MACRO: pause \pause
#+MACRO: runhtml $3
#+MACRO: onlyhtml 
#+MACRO: htmlonly 
#+MACRO: onlypdf $1
#+MACRO: pdfonly $1
#+MACRO: pdfhtml $1

#+MACRO: \begin{note}
#+MACRO: \end{note} 


# ##################################################################

#+LATEX_HEADER: \usepackage{version}

#+LATEX_HEADER: \AtBeginSection[]{\begin{frame}\frametitle{Plan}\setcounter{tocdepth}{3}\tableofcontents[currentsection]\end{frame}}
#+LATEX_HEADER: \definecolor{electricblue}{HTML}{05ADF3}
#+LATEX_HEADER: \hypersetup{colorlinks,pdfcreator=Erwan Jahier,pdfnewwindow=true}



#+OPTIONS: LaTeX:dvipng
#+DATE: Verimag
* Forewords
** Motivations: testing reactive programs

{{{pdfhtml(
#+begin_latex
\input{demarche-it-en}
#+end_latex,
file:./demarche-it-en.png
)}}}


** Lutin in one slide

- Lustre-like: Dataflow, parallelism, modular, logic time, =pre=.
- But not exactly Lustre though
 - Plus
  - *Control structure* operators (regular expressions)
  - *Stochastic* (controlled and pseudo-Aleatory) 

 - Minus
  - No implicit top-level loop 
  - No topological sort of equations

** In order to run this tutorial
 
You first need to install opam. For instance, on debian-like boxes do

#+BEGIN_SRC sh
 sudo apt-get install opam 
 opam init ; opam switch 4.04.0 ; eval `opam config env`
#+END_SRC

 and then do:

#+BEGIN_SRC sh
 sudo apt-get install gnuplot tcl
 opam repo add verimag-sync-repo "http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/opam-repository"
 opam update
 opam install lutin
#+END_SRC

and also the Lustre V4 distribution (for luciole and sim2chro) 
http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lustre-v4/distrib/index.html




{{{runpdf(./sh/echo-go.sh, echo "go!")}}}


* Execute Lutin programs 

# \includegraphics[width=10cm]{file:lutin-luciole.png}

** Stimulate Lutin programs


*** A program that increments its input

Let's consider the following Lutin program named [[./incr.lut][incr.lut]].
#+INCLUDE: "./incr.lut" src lutin

#+begin_src sh :tangle sh/incr-demo.sh :exports none :noweb yes
  xterm -hold -fa "Liberation Mono:size=15:antialias=false" -e "lutin incr.lut"
#+end_src

{{{run(./sh/incr-demo.sh, lutin incr.lut)}}}


\pause
*** A program with no input
 
# Consider the [[./one.lut][one.lut]] program:

#+INCLUDE: "./one.lut" src lutin

#+begin_src sh :tangle sh/one-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 5 one.lut"
#+end_src

{{{run(./sh/one-demo.sh, lutin -l 5 one.lut)}}}


*** Be quiet

#+begin_src sh :tangle sh/one-q-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 5 -quiet one.lut"
#+end_src

{{{run(./sh/one-q-demo.sh, lutin -l 5 -quiet one.lut)}}}


\pause

** Stimulate Lutin programs graphically with ~luciole~

#+INCLUDE: "./incr.lut" src lutin

#+begin_src sh :tangle sh/incr-luciole-demo.sh :exports none :noweb yes
  xterm -e "luciole-rif lutin -rif incr.lut"
#+end_src


# \href{run:./sh/incr-demo.sh}{\texttt{\color{DarkGreen}{ desktop  XXX}}} 


{{{run(./sh/incr-luciole-demo.sh, luciole-rif lutin incr.lut)}}}


** Store and Display the produced data: ~sim2chro~ and ~gnuplot-rif~

*** Generate a RIF file

It is  possible to store the lutin  RIF output into a  file using the
~-o~ option.

#+begin_src sh :tangle sh/rif-demo.sh :exports none :noweb yes
  xterm -e "lutin -l 10 -o ten.rif N.lut"
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "ls -lh ten.rif"
#+end_src

{{{run(./sh/rif-demo.sh, lutin -l 10 -o ten.rif N.lut ; ls -lh ten.rif)}}}

#+INCLUDE: "./N.lut" src lutin




\pause

*** Visualize a  RIF file 
#+begin_src sh :tangle sh/cat-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "cat ten.rif"
#+end_src
{{{run(./sh/cat-demo.sh, cat ten.rif)}}}


\pause

*** Visualize a  RIF file (bis)

#+begin_src sh :tangle sh/sim2chro-demo.sh :exports none :noweb yes
 cat ten.rif | sim2chrogtk -ecran > /dev/null
#+end_src
{{{run(./sh/sim2chro-demo.sh, cat ten.rif | sim2chrogtk -ecran > /dev/null)}}}

\pause

*** Visualize a  RIF file (ter)

#+begin_src sh :tangle sh/gnuplot-demo.sh :exports none :noweb yes
 gnuplot-rif ten.rif
#+end_src

{{{run(./sh/gnuplot-demo.sh,gnuplot-rif ten.rif)}}}


** Automatic stimulation of Lutin programs

#+INCLUDE: "./decr.lut" src lutin

#+begin_src sh :tangle sh/lurette-demo.sh :exports none :noweb yes
 lurette -sut "lutin decr.lut -n incr" -env "lutin decr.lut -n decr" -o res.rif &&\ 
 sim2chrogtk -ecran -in res.rif> /dev/null
#+end_src

{{{run(./sh/lurette-demo.sh, 
lurette -sut "lutin decr.lut -n incr" -env "lutin decr.lut -n decr" -o res.rif)}}}

{{{run(./sh/lurette-demo.sh, 
sim2chrogtk -ecran -in res.rif > /dev/null)}}}


{{{htmlonly([[./png/lurette-sim2chro.png]])}}}
\pause 


# \vspace{-4cm}
{{{pdfhtml(
\includegraphics[width=8.5cm]{jpg/geluck-echec.jpg},
file:jpg/geluck-echec.jpg)}}}

- I've bought 2 electronic chess games
- connected one to another
- And now I'm at peace

  
* The Lutin Language


** Back to programs of Section 1

- Let's come back to the Lutin  programs mentioned so far. 
#+INCLUDE: "./incr.lut" src lutin


- Those programs illustrate the 2 kinds of expressions we have in Lutin.
  -  *constraint  expressions* (~y = x+1~) that  asserts facts outputs
     variables. 
  - *trace expression* (~loop~ <te>) that allows one to combine 
    constraint expressions.



** Non deterministic programs

*** A First non-deterministic program


#+INCLUDE: "./trivial.lut" src lutin

#+begin_src sh :tangle sh/trivial-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 10 -q trivial.lut"
#+end_src

{{{run(./sh/trivial-demo.sh, lutin -l 10 -q trivial.lut)}}}


\pause
*** It is possible to set the variable range at declaration time,
as done in [[file:trivial2.lut][trivial2.lut]]:

#+INCLUDE: "./trivial2.lut" src lutin


#+begin_src sh :tangle sh/trivial2-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 10 -q trivial2.lut"
#+end_src

{{{run(./sh/trivial2-demo.sh,lutin -l 10 -q trivial2.lut)}}}


** Non deterministic programs (cont)

 Now consider the [[file:range.lut][range.lut]] program:

#+INCLUDE: "./range.lut" src lutin

#+begin_src sh :tangle sh/range-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin range.lut -l 10 -q"
#+end_src

{{{run(./sh/range-demo.sh, lutin range.lut -l 10 -q)}}}


{{{onlypdf(
- Linear constraints \to union  of convex polyhedra
- Several heuristics are defined to perform the solution  draw
)}}}

 - =--step-inside= (=-si=): draw inside the polyhedra 
  (the default)
 - =--step-vertices= (=-sv=) draw among the polyhedra vertices 
 - =--step-edges= (=-se=): promote edges 


#+begin_src sh :tangle sh/range-sv-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin range.lut -q -l 10 -sv "
#+end_src

{{{run(./sh/range-sv-demo.sh, lutin range.lut -l 10 -q --step-vertices)}}}


{{{onlyhtml([[file:range2.lut][Answer]])}}}

** Non deterministic programs (cont)

- A 3D non-deterministic example 

{{{onlyhtml(Of course constraints can be more complex as in [[file:polyhedron.lut][polyhedron.lut]]:)}}}

#+INCLUDE: "./polyhedron.lut" src lutin

#+begin_src sh :tangle sh/poly-demo.sh :exports none :noweb yes
  xterm  -e "lutin polyhedron.lut -l 1000 -quiet > poly.data";
  echo 'set pointsize 0.2 ; splot "poly.data" using 1:2:3;pause mouse close'| gnuplot
#+end_src

{{{run(./sh/poly-demo.sh, lutin polyhedron.lut -l 1000 -q > poly.data;\\ 
echo 'set point 0.2; splot "poly.data" using 1:2:3;pause mouse close'| gnuplot)}}}



#+begin_src sh :tangle sh/poly-se-demo.sh :exports none :noweb yes
  xterm -e "lutin polyhedron.lut -se -l 1000 -quiet > poly-se.data" &&
  echo 'set point 0.2 ; splot "poly-se.data" using 1:2:3; pause mouse close'| gnuplot
#+end_src

#+begin_src sh :tangle sh/poly-sv-demo.sh :exports none :noweb yes
  xterm -e "lutin polyhedron.lut -sv -l 1000 -quiet > poly-sv.data" &&
  echo 'set point 0.8; splot "poly-sv.data" using 1:2:3; pause mouse close'| gnuplot
#+end_src


- One   can   observe   the    effect   of  
    {{{nprun(./sh/poly-se-demo.sh,--step-edges)}}}
and
{{{nprun(sh/poly-sv-demo.sh,--step-vertices)}}})
options on the repartition of generated points



** Non deterministic programs (cont)


Constraint   may   also  depend   on   inputs.   

#+INCLUDE: "./range-bis.lut" src lutin
#+begin_src sh :tangle sh/range-bis-demo.sh :exports none :noweb yes
  xterm  -e "luciole-rif lutin range-bis.lut"
#+end_src

{{{run(./sh/range-bis-demo.sh, luciole-rif lutin range-bis.lut)}}}


** Controlled non-determinism: the choice operator
#+INCLUDE: "./choice.lut" src lutin

#+begin_src sh :tangle sh/choice-demo.sh :exports none :noweb yes
  xterm  -hold  -fa "Liberation Mono:size=15:antialias=false" -e "lutin -l 10 -q choice.lut";
#+end_src

{{{run(./sh/choice-demo.sh, lutin -l 10 -q choice.lut)}}}

\pause
It is possible to favor one branch over the other using 
weight directives (=:3=):

#+INCLUDE: "./choice2.lut" src lutin

In [[file:choice2.lut][choice2.lut]], ~x=42~ is chosen with a probability of 3/4.

 
#+begin_src sh :tangle sh/choice2-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 10000 -q choice2.lut | grep 42 | wc -l";
#+end_src

{{{run(./sh/choice2-demo.sh, lutin -l 10000 -q choice2.lut | grep 42 | wc -l)}}}


** Controlled non-determinism: the choice operator

#+INCLUDE: "./choice3.lut" src lutin
#+begin_src sh :tangle sh/choice3-demo.sh :exports none :noweb yes
  xterm  -e "luciole-rif lutin choice3.lut";
#+end_src

{{{run(./sh/choice3-demo.sh, luciole-rif lutin choice3.lut)}}}

** Combinators

 A /combinator/ is a well-typed  macro that eases code reuse.  
 One can define  a combinator with the =let/in=  statement, or just
 =let= for top-level combinators.


*** A simple combinator

#+INCLUDE: "./letdef.lut" src lutin

#+begin_src sh :tangle sh/letdef-demo.sh :exports none :noweb yes
xterm -hold  -fa "Liberation Mono:size=15:antialias=false" -e "lutin -quiet letdef.lut"
#+end_src

{{{run(./sh/letdef-demo.sh, lutin -quiet letdef.lut)}}}

** A parametric combinator 

The [[file:combinator.lut][combinator.lut]] program illustrates the use of parametric combinators:

#+INCLUDE: "./combinator.lut" src lutin

#+begin_src sh :tangle sh/combinator-demo.sh :exports none :noweb yes
rm -f walk.rif ; lutin  -l  100  combinator.lut  -o walk.rif && \
gnuplot-rif walk.rif
#+end_src

{{{run(./sh/combinator-demo.sh, lutin -l 100 combinator.lut -o walk.rif ; gnuplot-rif walk.rif)}}}


** Combinators  (cont)

*** A combinator that needs memory   (=ref=)  


#+INCLUDE: "./up-and-down.lut" src lutin

#+begin_src sh :tangle sh/up-and-down-demo.sh :exports none :noweb yes
luciole-rif lutin up-and-down.lut
#  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin up-and-down.lut"
#+end_src
#+begin_src sh :tangle sh/gnuplot-rif-luciole.sh :exports none :noweb yes
#  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "
gnuplot-rif luciole.rif
#+end_src

{{{run(./sh/up-and-down-demo.sh, luciole-rif lutin up-and-down.lut)}}}

{{{run(./sh/gnuplot-rif-luciole.sh, gnuplot-rif luciole.rif)}}}



\pause 
#+BEGIN_QUOTE
*Question:* what  happens if you  guard the =up= combinator  by =x<max=
instead of =pre x < max=?
#+END_QUOTE

** Local variables
   
Sometimes,  it is  useful to  use  auxiliary variables  that are  not
output  variables.    Such  variables  can  be   declared  using  the
~exist/in~    construct.    Its   use    is   illustrated    in   the
[[file:true-since-n-instants.lut][true-since-n-instants.lut]] program:

#+INCLUDE: "./true-since-n-instants.lut" src lutin
#+begin_src sh :tangle sh/true-since-demo.sh :exports none :noweb yes
luciole-rif lutin true-since-n-instants.lut
#+end_src

{{{run(./sh/true-since-demo.sh, luciole-rif lutin true-since-n-instants.lut)}}}


** Local variables again
Local variables  can also  plain random  variables, as
illustrated the [[file:local.lut][local.lut]] program:

#+INCLUDE: "./local.lut" src lutin


#+begin_src sh :tangle sh/local-demo.sh :exports none :noweb yes
 rm -f local.rif; lutin local.lut -l 100 -o local.rif && gnuplot-rif local.rif
#+end_src
#+begin_src sh :tangle sh/local-damped.sh :exports none :noweb yes
 rm -f local-bis.rif; lutin local-bis.lut -l 100 -o local-bis.rif && gnuplot-rif local-bis.rif
#+end_src

{{{run(./sh/local-demo.sh,lutin local.lut -l 100 -o local.rif ; gnuplot-rif local.rif)}}}


 *Question:* modify  the previous program  so that x reaches  the target
    after a
  {{{nprun(./sh/local-damped.sh,damped oscillation)}}}



** Damped oscillation
#+INCLUDE: "./local-bis.lut" src lutin

** Distribute a constraint into a scope: =assert=


Consider for instance  the  [[file:true-since-n-instants2.lut][true-since-n-instants2.lut]] program:

#+INCLUDE: "./true-since-n-instants2.lut" src lutin

- One flaw is that ~res = (b and (cpt<=0))~ is duplicated.

 
- ~assert <ce> in <te>~ \equiv ~<te'>~, 

     where <te'>= <te>[c/c and ce]$_{\forall c \in \mathcal{C}onstraints(te)}$


#+BEGIN_QUOTE
 *Question:* Rewrite the [[file:true-since-n-instants2.lut][true-since-n-instants2.lut]] using the
   ~assert/in~ construct and avoid code duplication.
#+END_QUOTE
  
 [[file:true_since_n_instants3.lut][Answer]]

** External code

Lutin program can call any function defined in a shared library (=.so=)

#+begin_src lutin :tangle ext-call.lut :exports none :noweb yes
extern sin(x: real) : real
let between(x, min, max : real) : bool = ((min < x) and (x < max))
node bizzare() returns (x,res: real) = 
  exist noise: real in
  assert between(noise,-0.1, 0.1) in
  res = 0.0 and x = 0.0 fby 
  loop     x = pre x + 0.1 + noise
       and res = sin(pre x)
#+end_src

#+INCLUDE "ext-call.lut" src lutin

#+begin_src sh :tangle sh/ext-call-demo.sh :exports none :noweb yes
 rm -f ext-call.rif
 lutin -L libm.so -l 200 -o ext-call.rif ext-call.lut && \
 gnuplot-rif ext-call.rif
#+end_src

{{{run(./sh/ext-call-demo.sh, lutin -L libm.so -l 200 ext-call.lut -o ext-call.rif;gnuplot-rif ext-call.rif)}}}


** Exceptions
*** Global exceptions can be declared outside the main node:

#+begin_src lutin
  exception ident
#+end_src 

*** or locally within a trace statement:
#+begin_src  lutin
exception ident in st
#+end_src

*** An existing  exception ident can  be raised with the  statement: 
#+begin_src lutin
raise ident
#+end_src

*** An exception can be caught with the statement:
#+begin_src lutin
catch ident in st1 do st2
#+end_src

If the exception is raised  in st1, the control immediatelly passes to
st2. If the "do" part is omitted, the statement terminates normally.

** Exceptions (cont)

- The predefined Deadlock exception can only be catched

#+begin_src lutin 
  catch Deadlock in st1 do st2
#+end_src
\equiv
#+begin_src lutin 
  try st1 do st2
#+end_src


- If a deadlock is raised during the execution of st1, the control
  passes immediately to st2.   If st1 terminates normally, the whole
  statement terminates and the control passes to the sequel.  

** Exceptions (cont)


#+INCLUDE "except.lut" src lutin
#+begin_src sh :tangle sh/except-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin except.lut"
#+end_src


{{{run(./sh/except-demo.sh, luciole-rif lutin except.lut)}}}


Note that the 43 value is  generated iff i=43.


** Combinators (again)
*** Trace Combinators

#+begin_src lutin
let myloop(t:trace) : trace = loop try loop t
#+end_src

Here we restart the loop from the beginning whenever we are
blocked somewhere inside =t=. ([[file:myloop.lut][myloop.lut]])

#+INCLUDE: "./myloop.lut" src lutin
#+begin_src sh :tangle sh/myloop-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin myloop.lut"
#+end_src

{{{run(./sh/myloop-demo.sh, luciole-rif lutin myloop.lut)}}}




** Parallelism: =&>=

# file:paralel.lut

#+INCLUDE: "./paralel.lut" src lutin 6-

#+begin_src sh :tangle sh/para-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin paralel.lut"
#+end_src

{{{run(./sh/para-demo.sh, luciole-rif lutin paralel.lut)}}}
 

nota bene: this construct can be expensive because of: 
- *the  control  structure*: such  a  product is  equivalent to  an
  automata product, which, in the worst case, can be quadratic;
- *the  data*:  the  polyhedron resolution  is  exponential in  the
  dimension of the polyhedron.
  
Use the =run/in= construct instead if performance is a problem.



# ** Parallelism *)

# In Lutin,  there are two  operators for computing things  in parallel: *)
# =&>= and =run=. *)

* The =run= operator  
** Cheap parallelism: Calling Lutin nodes =run/in=

 The idea  is the following: when one writes:

#+begin_src lutin
  run (x,y) := foo(a,b) in
#+end_src

in order to be accepted the following rules must hold:
- =a= and =b= be uncontrollable variables (e.g., inputs or memories)
- =x= and =y= should be controllable variables (e.g., outputs or locals)
- in the scope of such a =run/in=, =x= and =y= becomes uncontrollable.
 
\pause
nb : it is exactly the parallelism of Lustre, with an heavier syntax.
In Lustre, one would simply write 
#+begin_src lustre
 (x,y)=foo(a,b);
#+end_src

Moreover in Lutin, the order of equations matters.

** Cheap parallelism: Calling Lutin nodes =run/in=

- The =run/in= construct is another (cheaper) way of executing code in parallel
- The only way of calling Lutin nodes.  
- Less powerful: constraints are not merged, but solved in sequence 

#+INCLUDE: "./run.lut" src lutin

#+begin_src sh :tangle sh/run-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "lutin -l 5 -q run.lut -m use_run"
#+end_src

{{{run(./sh/run-demo.sh,lutin -l 5 -q run.lut -m use\_run)}}}





# ** Expensive versus cheap parallelism

#** Nodes versus combinators 
   
** Why does the =run/in= statement is important?
   
Using combinators and =&>=, it was already possible to reuse code, but
=run/in= is

- Much more efficient: polyhedra dimension is smaller
  
#Consider this example : xxx

- Mode-free (args can be in or out) combinators are error-prone
#Consider this example : xxx

 
* Advanced examples 
    
** Wearing sensors

The [[file:sensors.lut][sensors.lut]]  program that  makes extensive use  fo the
run statements.

#+begin_src sh :tangle sh/sensors-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin sensors.lut -m main"
#+end_src

{{{run(./sh/sensors-demo.sh,luciole-rif lutin sensors.lut -m main)}}}


** Waiting for the stability of a signal


- Defining and checking the stability of a variable (in particular in
  presence of noise) is not that easy.

- One  definition could be that  a variable is stable  if it remains
  within  an  interval  during  a  certain  amount  of  time.   More
  precisely:


#+begin_quote
A variable V  is (d,\epsilon)-stable at instant i  if there exists an
interval I of size \epsilon, such that, for all n in [i-d,i], V(n) is
in I.

\exists I, st |I| = \epsilon, \forall n \in [i-d, i] V(n) \in I
#+end_quote



[[file:is_stable.lut][The Lutin version]]

#+begin_src sh :tangle sh/is_stable-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "luciole-rif lutin is_stable.lut -m is_stable"
#+end_src

{{{run(./sh/is_stable-demo.sh,luciole-rif lutin is\_stable.lut -m is\_stable)}}}


** The Crazzy rabbit :noexport:
   
XXX pour  l'instant je  ne sais pas  a appeler du  ocaml via  rdbg en
ligne  de commande.  Et ai-je  vraiment  envie de  le faire  ? Ca  va
compliquer rdbg.ml grave. Bref, pour refaire marcher ce truc, il faudra
que j'ecrire une session.ml a la main

- The rabbit serves as an environment for a caml program
  that displays its position in a graphical windows
- The rabbit remains in its field, and avoids a moving obstable
- The rabbit changes it speed an trajectory from times to times

#+begin_src sh :tangle sh/rabbit-demo.sh :exports none :noweb yes
  xterm -hold  -fa "Liberation Mono:size=15:antialias=false"  -e "cd crazy-rabbit; lurettetop -rp "sut:ocaml:rabbit.cmxs:" -rp 'env:lutin:rabbit.lut:-main:rabbit:-L:libm.so:-loc'"
lurette -sut "" -env "lutin crazy-rabbit/rabbit.lut -n rabbit -L libm.so"

#+end_src

{{{run(sh/rabbit-demo.sh,lurettetop -rp "sut:ocaml:rabbit.cmxs:" -rp 'env:lutin:rabbit.lut:-main:rabbit:-L:libm.so:-loc')}}}


- [[file:crazy-rabbit/ud.lut][crazy-rabbit/ud.lut]]
- [[file:crazy-rabbit/moving-obstacle.lut][crazy-rabbit/moving-obstacle.lut]]
- [[file:crazy-rabbit/rabbit.lut][crazy-rabbit/rabbit.lut]]
- [[file:crazy-rabbit/rabbit.ml][crazy-rabbit/rabbit.ml]]

  
