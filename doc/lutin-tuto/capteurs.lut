-- Environnement où l'on simule des capteurs

let between(x, min, max : real) : bool = ((min < x) and (x < max))

node up(init, delta : real) returns(x : real) = 
   x = init fby loop { between(x, pre x, pre x + delta) }

node down(init, delta : real) returns(x : real) = 
   x = init fby loop { between(x, pre x - delta, pre x) }
   
node up_and_down(min, max, delta : real) returns (x : real) =   
  between(x, min, max)
  fby
  loop {
    | run x := up(pre x, delta)   in loop { x < max }
    | run x := down(pre x, delta) in loop { x > min }
  }


node up_and_down_alt(min, max, delta : real) returns (x : real) =   
  exist target : real in 
    between(x, min, max) and x = target
  fby
  loop {
      x = pre x and
      between(target, min, max) and 
      between(target, pre target - delta, pre target + delta) 
    fby
    loop [10,20] { 
       x = (pre x + target) / 2.0 and 
       target = pre target
    }
 }

node simu_capteur (T : real) 
  returns (Ts : real; invalidity : bool) =

  exist eps : real [-0.1; 0.1] in
  
  loop {
    -- capteur fonctionnant correctement
    loop [50, 100] {
      Ts = T + eps and invalidity = false
    }    
    fby
    -- capteur en panne
    {
      | -- bagottement
        loop [20, 30] Ts = T + eps
 
      | -- capteur bloque
        loop [20, 30] Ts = pre Ts and invalidity = false

      | -- invalidite remontee a faux
        loop [20, 30] Ts = T + eps and invalidity = true

      | -- capteur hors-plage
        loop [20, 30] Ts = 1000.0 and invalidity = false

      | -- capteur fou
        loop [20, 30] between(Ts, 95.0, 505.0) and invalidity = false
    }
  }

  

node main (
      A101MNAVALUE,
      A102MNAVALUE,
      A103MNAVALUE,
      A104MNAVALUE : real)
  returns (
      A101MNINV_VALUE, 
      A102MNINV_VALUE, 
      A103MNINV_VALUE, 
      A104MNINV_VALUE : bool;
      A101MNVALUE, 
      A102MNVALUE,
      A103MNVALUE, 
      A104MNVALUE : real) =

  exist temp : real in

  run temp := up_and_down(95.0, 505.0, 10.0) in
  run A101MNVALUE, A101MNINV_VALUE := simu_capteur(temp) in  
  run A102MNVALUE, A102MNINV_VALUE := simu_capteur(temp) in  
  run A103MNVALUE, A103MNINV_VALUE := simu_capteur(temp) in  
  run A104MNVALUE, A104MNINV_VALUE := simu_capteur(temp)