#+MACRO: run    #+begin_latex\\n\href{file:$1}{\texttt{\color{DarkGreen}{<prompt> $2}}} \n#+end_latex
#+MACRO: nprun  #+begin_latex\\n\href{file:$1}{\texttt{\color{DarkGreen}{$2}}} \n#+end_latex


#+MACRO: pause \pause

#+MACRO: runhtml $3
#+MACRO: onlyhtml 
#+MACRO: onlypdf $1
