;(package-initialize)
(setq load-path (cons (expand-file-name "~jahier/dd/org-mode/share/emacs/site-lisp/") load-path))
(setq load-path (cons (expand-file-name "/local/jahier/org-mode/") load-path))

(setq load-path (cons (expand-file-name "~/el_files/") load-path))


(setq load-path (cons (expand-file-name "~/.emacs.d/elpa/htmlize-20171017.141/") load-path))
;(setq load-path (cons (expand-file-name "/local/jahier/.emacs.d/elpa/htmlize-20161211.1019") load-path))
;(setq load-path (cons (expand-file-name "/local/jahier/.emacs.d/elpa/zenburn-theme-2.1/") load-path))

(require 'org)
;(require 'org-list)
;(require 'org-latex)
(require 'htmlize)
(require 'ob)
(require 'ob-lutin)
(require 'zenburn)

;(load-theme 'zenburn)
;(require 'ox-beamer)

(font-lock-fontify-buffer)

;(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)

(setq org-export-latex-listings t)
(add-to-list 'org-export-latex-packages-alist '("" "listings"))
(add-to-list 'org-export-latex-packages-alist '("" "color"))


(setq org-export-headline-levels 2)

(setq org-src-fontify-natively t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (ocaml . t)
   (lutin . t)
   (rif . t)
   (sh . t)
   )
 )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;%-----------------------------------------------------------------------%
;;; lang mode

(setq auto-mode-alist (cons '("\\.rif$"  . rif-mode) auto-mode-alist))
(autoload 'rif-mode "rif" "Major mode for viewing RIF outputs" t)
(setq auto-mode-alist (cons '("\\.sh$" . sh-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.lus$" . lustre-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.lut$" . lutin-mode) auto-mode-alist))
(autoload 'lutin-mode "lutin" "Edition de code lutin" t)
(setq auto-mode-alist (cons '("\\.ml\\w?" . tuareg-mode) auto-mode-alist))
(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code" t)

(setq org-export-html-postamble-format "Last Updated %d. Created by %c")
(autoload 'lustre-mode "lustre" "Edition de code lustre" t)


(setq auto-mode-alist '(
                        ("\\.org$"        . org-mode)
                        ("\\.txt$"        . text-mode)
                        ("\\.tex$"        . slitex-mode)
                        ("\\.sty$"        . latex-mode)
                        ("\\.sh$"              . sh-mode)
                        ("\\.csh$"              . sh-mode)
                        ("\\.lus$"      . lustre-mode)
                        ("\\.xec$"      . lustre-mode)
                        ("\\.saofd$"      . lustre-mode)
                        ("\\.lut$"      . lutin-mode)
                        ("\\.ima$"      . ima-mode)
                        ("\\.ec$"      . lustre-mode)
                        ("\\.java$"       . java-mode)
                        ("\\.jj$"       . java-mode)
                        ("\\.h$"        . c-mode)
                        ("\\.c$"        . c-mode)
                        ("\\.cc$"        . c-mode)
                        ("\\.cpp$"        . c-mode)
                        ("\\.rml[i]?" . tuareg-mode) ;; caml-mode)
                        ("\\.ml[iylp]?" . tuareg-mode) ;; caml-mode)
                        ("\\.[hg]s$"    . haskell-mode)
                        ("\\.hi$"       . haskell-mode)
			))


;This adds syntax highlighting to org-babel html exports. 
;(require-package 'htmlize)
;(setq org-export-htmlize-output-type 'css)

; To regenerate syntax highlighting CSS (for the active theme): 
;(org-export-htmlize-generate-css)
;;-----------------------------------------------------------------------


(setq inhibit-startup-message t) 

(custom-set-variables
; '(custom-enabled-themes (quote (zenburn)))
 '(custom-safe-themes (quote ("9f443833deb3412a34d2d2c912247349d4bd1b09e0f5eaba11a3ea7872892000" "71b172ea4aad108801421cc5251edb6c792f3adbaecfa1c52e94e3d99634dee7" "3580fb8e37ee9e0bcb60762b81260290329a97f3ca19249569d404fce422342f" default)))
)


;(require 'ox-latex)
;(add-to-list 'org-latex-classes
;             '("beamer"
;               "\\documentclass\[presentation\]\{beamer\}"
;               ("\\section\{%s\}" . "\\section*\{%s\}")
;               ("\\subsection\{%s\}" . "\\subsection*\{%s\}")
;               ("\\subsubsection\{%s\}" . "\\subsubsection*\{%s\}")))

(defvar my-website-html-postamble 
  "<div class='footer'>
Last updated %C. <br>
Built with %c.
</div>")


(setq org-publish-project-alist
      `(("org"
         :html-postamble ,my-website-html-postamble)))

(setq org-export-html-auto-postamble 'nil)
(setq org-export-html-postamble "Your postamble here")
(setq org-export-allow-bind-keywords t)

 (setq org-export-allow-BIND 1)
