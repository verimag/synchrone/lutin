(require 'org-latex)
(require 'org-special-blocks)

(setq org-export-latex-listings t)
(add-to-list 'org-export-latex-packages-alist '("" "listings"))
(add-to-list 'org-export-latex-packages-alist '("" "color"))

;; Resout le conflit entre amsmath et wasysym
(add-to-list 'org-export-latex-packages-alist '("" "amsmath" t))
(setcar (rassoc '("wasysym" t) org-export-latex-default-packages-alist)	"integrals")

(setq org-export-allow-BIND t)

(add-to-list 'org-export-latex-classes
             '("verimagreport"
               "\\documentclass[twoside, titlepage]{article}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))


(setq verimagreport-class
      '("verimagreport" "\\documentclass[twoside, titlepage]{article}"
        ("\\section{%s}" . "\\section*{%s}")
        ("\\subsection{%s}" . "\\subsection*{%s}")
        ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
        ("\\paragraph{%s}" . "\\paragraph*{%s}")
        ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(require 'org-latex)
(add-to-list 'org-export-latex-classes verimagreport-class t)

;(require 'org-e-latex)
;(add-to-list 'org-e-latex-classes verimagreport-class t)
