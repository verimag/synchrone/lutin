\contentsline {section}{\numberline {1}Introduction - Motivation}{2}{section.1}
\contentsline {section}{\numberline {2}Principles}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Environment}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}The Test Verdict}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Coverage}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}The Lurette overall process}{5}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}What's new in Lurette V3}{6}{subsection.2.5}
\contentsline {section}{\numberline {3}The Lurette command interpreter (\texttt {lurettetop})}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}The interactive command interpreter}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}The resource file (\texttt {.luretterc})}{8}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}The batch command interpreter}{8}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Defining coverage in Lurette}{8}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}The coverage file (\texttt {.cov})}{8}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}The Lurette dataflow}{9}{subsubsection.3.3.3}
\contentsline {section}{\numberline {4}Third-party tools provided in the distribution}{11}{section.4}
\contentsline {subsection}{\numberline {4.1}Programming stochastic Reactive machines (\texttt {lutin})}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Interactive simulation (\texttt {luciole})}{11}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Data visualisation (\texttt {sim2chro})}{11}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Gnuplot-based data visualisation (\texttt {gnuplot-rif})}{11}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Post-mortem coverage analysis: (\texttt {check-rif})}{11}{subsection.4.5}
\contentsline {section}{\numberline {5}A small tutorial}{12}{section.5}
\contentsline {section}{\numberline {6}Advanced used}{12}{section.6}
\contentsline {subsection}{\numberline {6.1}The liosi API}{12}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}The DLL/SO plugin API}{12}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}The socket plugin API}{12}{subsection.6.3}
