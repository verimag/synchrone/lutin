% Created 2013-10-08 Tue 17:33
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{soul}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage[integrals]{wasysym}
\usepackage{latexsym}
\usepackage{amssymb}
\usepackage{hyperref}
\tolerance=1000
\usepackage{amsmath}
\usepackage{color}
\usepackage{listings}
\input{comon-header}  
\providecommand{\alert}[1]{\textbf{#1}}

\title{The Lurette V3 User guide}
\author{Erwan Jahier}
\date{\today}
\hypersetup{
  pdfkeywords={},
  pdfsubject={},
  pdfcreator={Emacs Org-mode version 7.8.11}}

\begin{document}

\maketitle

\setcounter{tocdepth}{3}
\tableofcontents
\vspace*{1cm}
\section{Introduction -  Motivation}
\label{sec-1}


Lurette is an automated testing tool targeting reactive systems.



\href{http://www-verimag.imag.fr/Lutin.html}{http://www-verimag.imag.fr/Lutin.html}
\href{http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lurette/lutin-tuto-html.html}{http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lurette/lutin-tuto-html.html}

The functional testing of  reactive systems raises specific problems.
We  adopt the  usual point  of  view of  synchronous
programming,  considering that  the behavior  of such  a system  is a
sequence of atomic  reactions --- which can  be either time-triggered
or event-triggered, or  both ---.  Each reaction  consists in reading
inputs, computing  outputs, and  updating the  internal state  of the
system. As  a consequence,  a tester has  to provide  test sequences,
i.e., sequences of input vectors.   Moreover, since a reactive system
is generally designed to control its environment, the input vector at
a  given  reaction  may  depend   on  the  previous  outputs.   As  a
consequence, input  sequences cannot be produced  off-line, and their
elaboration must  be \emph{intertwined  with the  execution of  the system under test} (SUT).  Finally, in order  to decide whether a given test
succeeds   or   fails,   the   sequence   of   pairs   (input-vector,
output-vector) can be provided to an observer \cite{HLR93} which acts
as an oracle at each reaction.

Let us illustrate this process with a very simple example: consider a
device whose role is to regulate  the water temperature of a tank, by
opening or closing a gate that  controls the arrival of hot water.  A
reaction consists in sampling the  water temperature, comparing it to
the  target temperature,  and sending  an order  to the  gate.  In  a
realistic input sequence,  the temperature is assumed  to increase at
some rate when the  gate in open, and to decrease  when it is closed.
Hence the input at a given reaction depends on the output sent at the
previous reaction.  The property to be  checked could be that, if the
target  temperature  did  not  change   during  a  given  delay,  the
temperature  should belong  to  a given  interval  around the  target
temperature.     This   global    property    of   the    combination
[system-environment]  can  be  checked  after  each  reaction  by  an
observer, which must have an internal  memory to count the delay from
the last target change.  Moreover,  from this property coverage point
of view, we  want to generate sequences where  the target temperature
do not change too often.

In the past, we proposed languages and tools to automate this testing
process \cite{RWN+98,JRB06}.  The  system under test is  a black box;
it can be any executable code,  able to perform on demand a reaction, 
i.e., read  inputs; do a  step; provide outputs.  The  environment is
modeled using  dynamically changing  constraints on  inputs described
using Lucky  \cite{RJR06}; we now  use a higher-level  language named
Lutin  \cite{RRJ08}.   The  oracle  is provided  as  an  observer  in
Lustre. The tool Lurette is then able to run automatically any number
of  arbitrarily  long test  sequences.   Each  step consists  of  (1)
executing one (stochastic) reaction  of the environment that provides
inputs to  the SUT  (2) executing  one reaction of  the SUT  with the
chosen inputs, (3) executing one reaction of the oracle observer with
the SUT  inputs and  outputs (and  stopping the  test if  the checked
property is violated),  and (4) looping to (1) using  the SUT outputs
as environment inputs.
\section{Principles}
\label{sec-2}

\label{lurette-func}
\subsection{Environment}
\label{sec-2-1}



The main  challenge to  automate the  testing process  is to  able to
automate the  generation of \emph{realistic} input  sequences to feed
the  SUT.  In  other  words,  we need  an  executable  model  of  the
environment which inputs  are the SUT outputs, and  which outputs are
the SUT inputs.


Note that  realistic input sequences  can not be  generated off-line,
since the SUT  generally influences the behaviour  of the environment
it is  supposed to control,  and vice-versa. Imagine, for  example, a
heater controller  which input  is the temperature  in the  room, and
which output is  a Boolean signal telling the heater  whether to heat
or not.

In the  first Lurette  prototype, the  SUT environment  behaviour was
described by  Lustre observers  which were specifying  what realistic
SUT inputs should be. In other words, the environment was modelled by
a set  of (linear)  constraints over  Boolean and  numeric variables.
The work  of Lurette was  to solve those  constraints, and to  draw a
value among the solutions to produce one SUT input vector.

But, from a language point of view, Lustre observers happen to be too
restrictive, in particular to  express sequences of different testing
scenarios, or to have some  control on the probabilistic distribution
of  the  drawn   solution.   It  was  precisely   to  overcome  those
limitations that a new language, Lutin, was designed.

For more  information on  Lutin, please refer  to the  Lutin language
reference manual\~{}\cite{lutin-man}. A tutorial is also available.
\subsection{The Test Verdict}
\label{sec-2-2}


The second thing that needs to be automated in the testing process is
the  test result  decision. In  other words,  we need  to be  able to
decide automatically whether or not  the test succeeded.  To do that,
we  will use  exactly  the same  technique as  in  the first  Lurette
prototype:  namely, via  the  use of  \emph{Lustre  observers}.  A  Lustre
observer  is  a  Lustre  program that  returns  exactly  one  Boolean
variable. It lets one express any  safety property -- but no liveness
property.

Users therefore  need to write an  observer which inputs are  the SUT
inputs and outputs, and which output is a single Boolean that is true
if and only if the test vectors are correct w.r.t.  a given property.
That property  can be,  for example, a  property that  a verification
tool failed  to prove --  which is precisely when  testing techniques
are useful.
\subsection{Coverage}
\label{sec-2-3}


\label{sec-cov}  

When performing functional testing,  structural coverage criteria are
insufficient to give insights about  whether or not enough tests have
been done.  We also need  coverage criteria attached to requirements.
Consider for  instance the following property,  which expresses that,
when a threshold  is exceeded, and when the system  is in its nominal
mode, then an alarm must be raised:
 

\begin{verbatim}
(T>100 and nominal) => Alarm
\end{verbatim}

There are several ways for the  SUT to satisfy this property: (1) \texttt{T}
can  be smaller  than 100  or (2)  the system  can be  in a  degraded
(non-nominal) mode;  (3) otherwise, \texttt{Alarm}  must be true.   From the
coverage  point of  view, it  is obviously  the latter  case that  is
interesting.   Its seems  fair to  consider that  this oracle  is not
covered as long as no simulation  has been run where \texttt{T>100} and both
\texttt{nominal} and \texttt{Alarm} are true.  The case \texttt{T<100} is also interesting
to cover, but can be attached to a property related to the absence of
false alarms.

Hence, we  define the  \emph{coverage of  an oracle} as  a set  of Boolean
conditions.  A \emph{run} (or  a \emph{trace}) of the SUT is  a sequence of the
SUT input/output  vectors generated during a  simulation.  The oracle
\emph{coverage rate of  a set of runs} is the  rate of coverage conditions
that have been true at least once during those runs.  The coverage of
a property is  arguably part of its specification.  If  it is not the
case, the persons in charge  of formalizing requirements into oracles
are in the best position to define the coverage at the same time.
\subsection{The Lurette overall  process}
\label{sec-2-4}


\label{req-eng}

The design of the system, its oracles, and its stimulus generators is
not a  linear process.   Several iterations  are necessary,  that are
described below, and outlined in Fig. \ref{fig:demarche}. 

\myparagraph{Refining the SUT} When an oracle is violated, it can be,
of course,  because of a design  or coding error, which  results in a
erroneous  SUT. Detecting  such  incorrect behaviors  of  the SUT  is
indeed the original motivation of all this infrastructure.

\myparagraph{Refining oracles} An oracle violation can also be due to
a wrong  formalization.  Despite  the fact that  sequence recognizers
(oracles) are much simpler to develop than sequence generators (SUT),
they are still the result of a human work and thus exposed to errors.


\myparagraph{Refining ambiguous requirements} Lurette can also detect
ambiguous requirements, when they  are interpreted differently by the
SUT and  the oracle designers.   It happened quite  frequently during
the project. 


\myparagraph{Refining    inconsistent    requirements}    Formalizing
requirements in  a language such  as Lustre  that is equipped  with a
model-checker  allows  to  detect  inconsistencies, i.e., the absence of
correct behaviors.    

\myparagraph{Refining  imprecise  requirements} Another  reason  that
results in  invalidated oracles is  when they are based  on imprecise
requirements.   One typical  case encountered  in the  project was  a
requirement formulated as follows:  ``when \texttt{x} exceeds the threshold
\texttt{t}, the alarm \texttt{a} should be raised''.  In a distributed system like
the  one  of COMON,  where  sub-systems  communicate over  buses  and
networks,  such  a  requirement   will  be  immediately  violated  if
interpreted  literally. One  should  permit some  reaction delay,  and
specify its bounds. 

\myparagraph{Refining  incomplete requirements}  Another very  common
source of  oracle violations  is a lack  of completeness.   A typical
example, also encountered  during the project, is  a requirement that
states that ``the temperature of tank \texttt{t} should never exceeds \texttt{100}
degrees'', whereas the correct requirement was ``the temperature of
tank \texttt{t} should never exceeds \texttt{100} degrees when the system is in the
nominal mode and the validity bit associated to its sensor is 1''.

One outcome of  the project is that the Lurette  tool and methodology
was actually helpful for debugging and refining requirements.

\myparagraph{Refining scenarios}  When the coverage is  not complete,
we must enrich the set of  possible behaviors of the environment with
new  scenarios.   Note that  new  scenarios  may lead  to  properties
violations, which  lead to changes  in the  SUT (or in  oracles); and
changes in the SUT may change the coverage in turn.


\begin{figure}[!ht]
\centering
\includegraphics[width=\linewidth,]{./demarche-it-en.pdf}
\caption{\label{fig:demarche}The Lurette iterative process loops.  Oracles and environments of the SUT are extracted from heterogeneous specifications.  When an oracle is invalidated, it can be due to a design error, a coding error, or to a wrong or imprecise specification.  Once the system is running without invalidating oracles, in order to improve the coverage rate, the tester needs to refine test scenarios.}
\end{figure}
\subsection{What's new in Lurette V3}
\label{sec-2-5}


While rewriting part  of the code of Lurette,  we've also changed the
the  way Programs  under test  are connected  to  Lurette.  Moreover,
we've changed  most command-line option names,  which breaks previous
Makefiles,  the Lurette  GUI.  As  a consequence,  the  manual become
obselete. For all  these reasons, we though that  make things clearer
to change the version number.


Since Makefiles that  were using the Hence, we choose  to start a new
version of  the tool :  lurette V3.  In  short, the main  change with
lurette V2 are the following.


\begin{itemize}
\item A new Lutin interpreter
\end{itemize}

Since the  last version  of Lurette (V2),  the Lutin  interpreter has
been rewritten.   Moreover, the language  has been extended  with the
\texttt{run} statement that  allows one to write more  efficient and modular
Lutin programs.

\begin{itemize}
\item A more flexible scheme
\end{itemize}

In V2,  one had to provide  one SUT (System under  test, typically in
Lustre  or Scade);  one  environment for  this SUT  (in  Lucky or  in
Lutin); and optionnaly,  one oracle (in Lustre).  In V3,  one can set
any number  of SUTs,  Environments, and  oracles.  Moreover,  each of
these entities  can be  in Lustre or  Lutin. They can  be C  or ocaml
programs provided  that follow some specific  convention as described
in Section \ref{api}.
 They can even be stand-alone programs that read/write their I/O using 
the RIF format (cf Section \ref{liosi}).


\begin{itemize}
\item A new plugin mechanism
\end{itemize}

In Lurette V2,  all the files were statically  compiled into a single
executable.   Lurette was  doing its  best to  generate automatically
that  executable,  taking  into  account  various  kinds  of  C  code
generated  by  various  compilers   (lustre  V4,  Lustre  V6,  Scade,
RT-builder, C, ocaml, etc.).

In Lurette V3, we made a  different choice: we ask users to provide a
dynamic  library  (in C  or  in  Ocaml)  that implements  a  specific
interface for each  reactive program he wants to  use within Lurette.
However, some tools are provided  in the Lurette distribution to help
users to generate such libraries,  but they are not integrated in the
Lurette core (they migth be used in the GUI though).

It  is  somewhat  a regression;  but  in  V2,  the will  to  automate
everything  was a real  pain when  something went  wrong in  the main
executable generation  process. The  idea here is  to write  for each
backends  (Lustre,  Scade,  etc.)   a  specific  stand-alone  dynamic
libraries  generator and to  keep this  job explicitely  separated of
Lurette.

\begin{itemize}
\item A property coverage mechanism
\end{itemize}



\newpage
\section{The Lurette command interpreter (\texttt{lurettetop})}
\label{sec-3}
\subsection{The interactive command interpreter}
\label{sec-3-1}


\lstset{language=sh}
\begin{lstlisting}
$ lurettetop
\end{lstlisting}
\subsection{The resource file (\texttt{.luretterc})}
\label{sec-3-2}


When  \texttt{lurettetop} is  launched, it  first reads  the content  of the
resource file named \texttt{.lurette\_rc} in the current directory. The syntax 
of this file is just the one of interactive command described above.


\lstset{language=sh}
\begin{lstlisting}
set_test_length 10
set_sut "heater_control.lus" "heater_control"
set_step_by_step_off
\end{lstlisting}
\subsection{The batch command interpreter}
\label{sec-3-3}

\texttt{lurettetop} can also run in batch mode
 

\lstset{language=sh}
\begin{lstlisting}
lurettetop --help
\end{lstlisting}

If a resource file exists in the current directory, \texttt{lurettetop} will
first  interpret its  content,  and then  interpret the  command-line
batch options (and thus override the \texttt{.lurette\_rc} commands).
\subsubsection{Defining coverage in  Lurette}
\label{sec-3-3-1}


In Lurette,  in order to define  the coverage of an  oracle, one just
needs to add additional Boolean  variables to its output profile.  By
convention,  the  first  output  holds the  oracle  result,  and  the
following outputs  define the  oracle coverage.  Lurette  updates the
coverage  rate from  one execution  to  another (via  a file).   This
coverage rate  is reset  each time  either the oracle  or the  SUT is
modified. 

cf Section \ref{cov-ann}.
\subsubsection{The coverage file (\texttt{.cov})}
\label{sec-3-3-2}

\label{cov-ann}

Lurette maintains the  coverage information via a  \texttt{.cov} file, which
looks like this:

\lstset{language=rif}
\begin{lstlisting}
SUT: v6:heater_control.lus:main
ORACLE: v4:heater_control.lus:not_a_sauna
ORACLE: v6:heater_control.lus:not_a_fridge
RIF: test.rif0 - generated at 16:28:15 the 18/4/2011 ; the coverage rate is 50.0%
RIF: test.rif0 - generated at 16:29:20 the 18/4/2011 ; the coverage rate is 60.0%
VAR: c1 t
VAR: c2 f
VAR: c3 t
VAR: c4 t
VAR: c5 f
VAR: c6 t
\end{lstlisting}

The coverage is a function from 
\begin{itemize}
\item a sut
\item an oracle
\item a set of runs (rif files).
\end{itemize}

The  header contains  information about  the  sut and  the oracle  we
measure the coverage of (\texttt{SUT:} and \texttt{ORACLE:}). Then comes the set of
runs that  have been performed  on them (\texttt{RIF:}). Finally,  comes the
list of coverage variables, and  their status indication if there had
been true at least once during a run (\texttt{t} meaning covered).

When no coverage file is specified, Lurette creates such a file using
as coverage conditions all the  Boolean outputs of oracles, the first
one excepted (as it is used to hold the test verdict).

If  one wants  to remove  a coverage  condition without  changing the
oracle profiles, it suffices to remove (or comment) the corresponding
line in this  coverage file. When the SUT or  the oracle changes, the
coverage is  reset. One can  also force the coverage  resetting using
the \texttt{-{}-reset-cov-file true} option (in batch mode) or the
\texttt{reset\_cov\_file true} commande (in interactive mode).

cf also the \texttt{check-rif} bacth tools in Section \ref{check-rif}.
\subsubsection{The Lurette dataflow}
\label{sec-3-3-3}


We recall now how the different entities (SUT, oracle, environment)
work all together inside Lurette.

\begin{figure}[h!]
\centering
\includegraphics[width=1\linewidth,]{../figs/lurette-v3.pdf}
\caption{\label{fig:dataflow}Lurette data flow. At each step, each orange box is activated from left to right. A Luciole process is used only if necessary (i.e., if a SUT or an ENV input is missing). At least one SUT or one ENV is necessary. The sim2chro process can be launched from Luciole, or post-mortem from the Lurette UI via the generated rif file.}
\end{figure}

 



Figure\~{}\ref{lurette-start} shows what happens when Lurette starts up.
\% Since the environment outputs  serve as SUT inputs, and SUT outputs
serve as environment inputs (the first step excepted), in order to be
able to start  such a looped design, one entity  have to start first.
The choice has been made  that the environment will.  This means that
a  valid environment  for Lurette  is  one that  can generate  values
without any input at the first instant.

The  role  of \texttt{boot}  keyword  of Figure\~{}\ref{lurette-start}  is
precisely to indicate that  the environment is indeed starting first;
once   the   environment  received   the \texttt{boot}  signal,   it
(non-deterministically) produces an output  vector $I$, which will be
used by the  SUT. $try \ I$ means  that once one step is  done in the
SUT  to compute  its output  $O$, the  previous state  of the  SUT is
immediately  restored. This allows  several input  vectors $I$  to be
tried  at each  step,  to perform  a  kind of  \emph{thick} test.  In
Figure\~{}\ref{lurette-start},  the  different  tries are  distinguished
thanks to  the index $i$. Hence,  at the $i^{th}$  tries, the vectors
$I_i$  and $O_i$, respectively  produced by  the environment  and the
SUT, are tried in the oracle.





Once a sufficient number of tries has been done (the thickness of the
test  is one of  the Lurette  parameters users  have control  on, cf.
Section\~{}\ref{test-parameters}), one index is chosen, say $j$, and the
step corresponding to that index is really\footnote\{is the sense that
the  previous state  of machines  is  not restored  this time.\}  done
(Figure\~{}\ref{lurette-step}). Note  that since the SUT  and the oracle
are  deterministic machines,  we just  need to  give them  the vector
$I_j$ and $O_j$ once more. But  this is not true for the environment,
which is  non-deterministic; that  is the reason  why we give  it the
index  $j$; of course,  this means  that the  environment interpreter
needs to remember which index led to which internal state.



Then the process continues as in Figure\~{}\ref{lurette-start}; the only
difference,  as  shown   in  Figure\~{}\ref{lurette-try},  is  that  the
environment is fed with the SUT output vector $O_j$ which was elected
at the previous step (instead of \texttt{boot i}).
\section{Third-party tools provided in the distribution}
\label{sec-4}

  
Some  of  the  \texttt{lurettetop}  commands actually  rely  on  stand-alone
executable  that can  be  used without  Lurette. Understanding  their
usage can be useful per se, and can also sometimes help to understand
what  is going  on  when  something bad  happens.  Indeed, the  error
messages produced by  Lurette are in fact sometimes  generated by one
of those tools.
\subsection{Programming stochastic Reactive machines (\texttt{lutin})}
\label{sec-4-1}
\subsection{Interactive simulation  (\texttt{luciole})}
\label{sec-4-2}


Sometimes it's  useful to have interactive  test sessions, typically
be before writing a first Lutin environment for a SUT.
\subsection{Data visualisation (\texttt{sim2chro})}
\label{sec-4-3}

\label{sim2chro}
\subsection{Gnuplot-based data visualisation (\texttt{gnuplot-rif})}
\label{sec-4-4}

\label{gnuplot-rif}
\subsection{Post-mortem coverage analysis:   (\texttt{check-rif})}
\label{sec-4-5}


\label{check-rif}
Lurette met à jour la couverture des test (par l'entremise du fichier
\texttt{.cov}) au fur  et à mesure que les tests se  déroulent.  Mais il est
également  possible  s'utiliser  un  petit  utilitaire  en  ligne  de
commande  :  \texttt{check-rif}. Cet  utilitaire  utilise  un  oracle et  un
fichier  .rif  généré lors  d'une  précédente  session  de test  pour
calculer (où  mettre à jour)  la couverture fonctionnelle  associée à
cet oracle. La commande \texttt{check-rif -help} explique tout ce qu'il y a
à savoir sur cet outil.
   
\section{A small tutorial}
\label{sec-5}
\section{Advanced used}
\label{sec-6}

\label{api}
\subsection{The liosi API}
\label{sec-6-1}


j'ai commencé la, en me basant sur le document de sdd
\href{file:///home/jahier/PROJECTS/COMON/org/liosi.org}{file:\~/PROJECTS/COMON/org/liosi.org}

il reste à traduire, et à trouver un moyen de présenter l'api
sans code.
\subsection{The DLL/SO plugin API}
\label{sec-6-2}
\subsection{The socket plugin API}
\label{sec-6-3}


Lurette  entities   (SUT,  oracles,  environments)  can   also  be  a
stand-alone  program that reads  and writes  its inputs/outputs  on a
socket.  Of course, such programs ougth to respect a precise protocol
that we describe below.
 
\begin{enumerate}
\item First it must \textbf{connect to  an inet socket} (defined by an address and
   a port), using the \verb~listen~ command;
\item Then it must write its \textbf{input variable names and types} (that will be
   received from the lurette process) using the \hyperref[RIF]{RIF} convention
\item Then it must write its \textbf{output variable names and types} (that will be
   send to lurette process) using the \hyperref[RIF]{RIF} convention
\item Then, it enter a loop where it
\begin{itemize}
\item reads its input on the socket (in their declaration order)
\item writes its output on the socket (in their declaration order)
\end{itemize}
\end{enumerate}
In  order to  stop  that loop  and  inform lurette  it  wants to  stop
playing, the program just have to send the \verb~#quit~ command.

Here a  small but  complete example of  a C  program that is  able to
communicate with Lurette (and that is part of its non-regression test
suite) :


 \# \href{file:///home/jahier/lurette/examples/lutin/lurette-socket/simple_sut.c}{file:\~jahier/lurette/examples/lutin/lurette-socket/simple\_sut.c}

\lstset{language=C}
\begin{lstlisting}
#include <stdlib.h>
#include <stdio.h> 
#include <sys/types.h>
#include <signal.h>
#include <locale.h>
#ifdef _WINSOCK
  #include <windows.h>
  #include <process.h>
  #pragma comment(lib, "Ws2_32.lib")
#else
  #include <sys/socket.h>
  #include <netinet/in.h>
  #include <netdb.h>
#endif
#define MAX_BUFF_SIZE 2048 // To be set with care!!
#define SOCK_ADDR "127.0.0.1"
#define SOCK_PORT 2000
typedef int _bool;
// A little program with 3 inputs, and 3 outputs, that dialogs with lurette on a socket.
int main(){
  int i = 0;
  int rc = 0;
  char buff [MAX_BUFF_SIZE];
  int sock, sockfd, newsockfd, clilen;
  struct sockaddr_in serv_addr, cli_addr;
  // The program Inputs:
  int a;  _bool b;  double c;
  char b_char; // used for reading booleans on the socket
  // The program Outputs:
  int x=0;  _bool y=0;  double z=0.0;
  // Socket administration 
#ifdef _WINSOCK
  WSADATA WSAData;
  WSAStartup(MAKEWORD(2,0), &WSAData);
#endif
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) printf("Error: opening socket");
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(SOCK_ADDR);
  serv_addr.sin_port = htons(SOCK_PORT);
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) ) {
    printf("Error when binding %s:%d\n", SOCK_ADDR, SOCK_PORT); exit(2); 
  } 
  listen(sockfd,5);
  clilen = sizeof(cli_addr);
  newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
  if (newsockfd < 0) printf("Error: on accept");
  sock = newsockfd;
  // Make sure that the program uses a "." for reals, and not a ","
  setlocale(LC_ALL, "English"); 
  // sending I/O declarations
  memset(buff, 0, MAX_BUFF_SIZE);
  sprintf(buff, "@#inputs \nx:int \ny:bool\n z:real \n#@\n"); // multi-line rif decl
  send(sock, buff, (int) strlen(buff),0); 
  sprintf(buff, "#outputs a:int b:bool c:real\n"); // single-line rif decl (just for fun)
  send(sock, buff, (int) strlen(buff),0); 
  for (i=1; i<10; i++){ // The main loop   
    // 1 - Reading inputs
    memset(buff, 0, MAX_BUFF_SIZE);
    rc = recv(sock, buff, MAX_BUFF_SIZE, 0);
    if (rc<0)  { printf("Error: cannot read on socket\n"); exit(2); };
    sscanf(buff, "%d %c %lf", &a, &b_char, &c);
    // Translate char into int
    if ((b_char == '0') || (b_char == 'f') || (b_char == 'F')) b = 0;
    if ((b_char == '1') || (b_char == 't') || (b_char == 'T')) b = 1;
    // 2 - Computing the outputs using the inputs
    x = a+1; if (b) { y = 0; } else { y = 1; }; c = z+0.1;  
    // 3 - Writing outputs 
    memset(buff, 0, MAX_BUFF_SIZE);
    sprintf(buff, "%d %d %lf \n", a, b, c);
    send(sock, buff, (int) strlen(buff),0);    
    // A small debug-printf to see what's going on...
    printf("#step %d \n%d %d %f #outs %d %d %f\n", i, a, b, c, x, y, z);
  }
  sprintf(buff,"#quit\n");
  send(sock, buff, (int) strlen(buff),0);    
  return 0;
}
\end{lstlisting}



\end{document}
\section{A standard Lutin library}
\label{sec-7}
\section{A Lustre oracle  library}
\label{sec-8}

\end{document}
