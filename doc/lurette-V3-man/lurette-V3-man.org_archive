#    -*- mode: org -*-


Archived entries from file /home/jahier/lurette/doc/lurette-V3-man/lurette-V3-man.org


* Principles
  :PROPERTIES:
  :ARCHIVE_TIME: 2012-03-19 Mon 14:11
  :ARCHIVE_FILE: ~/lurette/doc/lurette-V3-man/lurette-V3-man.org
  :ARCHIVE_CATEGORY: lurette-V3-man
  :END:
\label{lurette-func}
 
 Who start playing?

#+LABEL:  fig:dataflow  
#+CAPTION: Lurette data flow. At each step, each orange box is activated from left to right. A Luciole process is used only id necessary (i.e., if a SUT or an ENV input is missing). At least one SUT or one ENV is necessary. The sim2chro process can launched from Luciole, or post-mortem from the Lurette UI via the generated rif file.
#+ATTR_LaTeX: width=1\linewidth,placement=[h!]
[[file:../figs/lurette-v3.pdf]]
 

** Describing and simulating the System Under Test (SUT) Environment: SUT Input sequence generation.

The main challenge to automate the testing process is to able to
automate the generation of \emph{realistic} input sequences to feed
the SUT. In other words, we need an executable model of the
environment which inputs are the SUT outputs, and which outputs are
the SUT inputs. 


Note that realistic input sequences can not be generated off-line,
since the SUT generally influences the behaviour of the environment it
is supposed to control, and vice-versa. Imagine, for example, a
heater controller which input is the temperature in the room, and
which output is a Boolean signal telling the heater whether to heat
or not.

In the first Lurette prototype, the SUT environment behaviour was
described by Lustre observers which were specifying what realistic
SUT inputs should be. In other words, the environment was modelled by
a set of (linear) constraints over Boolean and numeric variables.
The work of Lurette was to solve those constraints, and to draw a
value among the solutions to produce one SUT input vector.

But, from a language point of view, Lustre observers
happen to be too restrictive, in particular to express sequences of
different testing scenarios, or to have some control on the
probabilistic distribution of the drawn solution.  It was precisely
to overcome those limitations that a new language, Lucky, was
designed. 



 The Lutin Language.

XXX Please refer to the Lutin language reference manual~\cite{lutin-man}
for more information.  


** The Test Oracle

The second thing that needs to be automated in the testing process is
the  test result  decision. In  other words,  we need  to be  able to
decide automatically whether or not  the test succeeded.  To do that,
we  will use  exactly  the same  technique  as in  the first  Lurette
prototype:  namely, via  the  use of  /Lustre  observers/.  A  Lustre
observer  is  a  Lustre  program  that returns  exactly  one  Boolean
variable. It lets one express  any safety property -- but no liveness
property.

 Users therefore  need to write an observer which
inputs are the  SUT inputs and outputs, and which  output is a single
Boolean that  is true  if and  only if the  test vectors  are correct
w.r.t.  a  given property.  That  property  can  be, for  example,  a
property  that  a verification  tool  failed  to  prove --  which  is
precisely when testing techniques are useful.


XXX can be lustre, scade, ... or anything else !


** The Lurette dataflow

We recall now how the different entities (SUT, oracle, environment)
work all together inside Lurette.



# \begin{figure}[h]
# %\vspace{-2cm}
# \begin{center}
#   \mbox{ \scalebox{.45}{\input{objs/lurette-start}}}
# \end{center}
#   \caption{Lurette start-up.}
#   \label{lurette-start}
# \end{figure}



Figure~\ref{lurette-start} shows what happens when Lurette starts up.
% Since the environment outputs  serve as SUT inputs, and SUT outputs
serve as environment inputs (the first step excepted), in order to be
able to start  such a looped design, one entity  have to start first.
The choice has been made  that the environment will.  This means that
a  valid environment  for Lurette  is  one that  can generate  values
without any input at the first instant.

The  role  of =boot=  keyword  of Figure~\ref{lurette-start}  is
precisely to indicate that  the environment is indeed starting first;
once   the   environment  received   the =boot=  signal,   it
(non-deterministically) produces an output  vector $I$, which will be
used by the  SUT. $try \ I$ means  that once one step is  done in the
SUT  to compute  its output  $O$, the  previous state  of the  SUT is
immediately  restored. This allows  several input  vectors $I$  to be
tried  at each  step,  to perform  a  kind of  \emph{thick} test.  In
Figure~\ref{lurette-start},  the  different  tries are  distinguished
thanks to  the index $i$. Hence,  at the $i^{th}$  tries, the vectors
$I_i$  and $O_i$, respectively  produced by  the environment  and the
SUT, are tried in the oracle.



# \begin{figure}[h]
#  \begin{minipage}{0.5\textwidth}
#   \mbox{ \scalebox{.45}{\input{objs/lurette-step}}}
#   \caption{Lurette steps \label{lurette-step}}
# \end{minipage}
# %
#  \begin{minipage}{0.5\textwidth}
#   \mbox{ \scalebox{.45}{\input{objs/lurette-try}}}
#   \caption{Lurette tries.}
#   \label{lurette-try}
# \end{minipage}

# \end{figure}




Once a sufficient number of tries has been done (the thickness of the
test  is one of  the Lurette  parameters users  have control  on, cf.
Section~\ref{test-parameters}), one index is chosen, say $j$, and the
step corresponding to that index is really\footnote{is the sense that
the  previous state  of machines  is  not restored  this time.}  done
(Figure~\ref{lurette-step}). Note  that since the SUT  and the oracle
are  deterministic machines,  we just  need to  give them  the vector
$I_j$ and $O_j$ once more. But  this is not true for the environment,
which is  non-deterministic; that  is the reason  why we give  it the
index  $j$; of course,  this means  that the  environment interpreter
needs to remember which index led to which internal state.



Then the process continues as in Figure~\ref{lurette-start}; the only
difference,  as  shown   in  Figure~\ref{lurette-try},  is  that  the
environment is fed with the SUT output vector $O_j$ which was elected
at the previous step (instead of =boot i=).


