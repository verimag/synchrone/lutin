#!/bin/sh

set -e

SHA=$(git log -1 --pretty=format:"%h" || echo "opam")
VERSION=$(git describe --tags || ls ../../../../../ | grep lutin | cut -d '.' -f2-4)
echo "let str0=\"${VERSION}\"" > version.ml
echo "let sha=\"${SHA}\"
let str () =
  if sha<>\"opam\" then str0 else
    match (Mypervasives.run \"opam info lutin -f version\" (fun s -> Some s)) with
    | [] -> \"?\"
    | v::_ -> v

" >> version.ml