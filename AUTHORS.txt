(* OASIS_START *)
(* DO NOT EDIT (digest: b02258ab91ae2df27b3896c979a325ed) *)

Authors of lutin:

* Erwan Jahier
* Pascal Raymond
* Bertrand Jeannnet (polka)
* Yvan Roux

Current maintainers of lutin:

* erwan.jahier@univ-grenoble-alpes.fr

(* OASIS_STOP *)
